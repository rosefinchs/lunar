﻿

#include <iostream>
#include "TestLunnar.h"
#include "LuaTable.h"

struct TestItem
{
	std::string name;
	double weight;
};

void TestLuaVar()
{
	using namespace lunar;


	LuaVar var0(false), var1(999), var2("hello"), var3(TestItem{"zhang", 34.56});


	printf("var0.luaType = %d, v = %s \n", var0.getLuaType(), var0.Cast<bool>()?"true":"false");
	printf("var1.luaType = %d, v = %d \n", var1.getLuaType(), var1.Cast<int>());
	printf("var2.luaType = %d, v = %s \n", var2.getLuaType(), var2.Cast<const char*>());
	printf("var3.luaType = %d, v = (%s, %f) \n\n", var3.getLuaType(), var3.Cast<TestItem>().name.c_str(), var3.Cast<TestItem>().weight);

	{
		LuaVar var8("hi");
		printf("var8.luaType = %d, v = %s \n", var8.getLuaType(), var8.Cast<const char*>());

		var8 = var1;
		printf("var8.luaType = %d, v = %d \n", var8.getLuaType(), var8.Cast<int>());
	}
	
	printf("end\n");
}

void TestLuaTable()
{
	using namespace lunar;

	auto ls = LuaRunner::OpenState();
	if (ls)
	{
		LuaRunner::RunLuaFile(ls, "test_table.lua");

		LuaVar var = LuaRunner::GetLuaVar(ls, "my_name");
		if (var.IsValid())
		{
			auto name = var.Cast<const char*>();
			printf("my_name = %s \n", name);
		}

		LuaTable tbl = LuaRunner::GetLuaTable(ls, "my_pos");
		if (!tbl.Empty())
		{
			auto x = tbl.GetItem("x").Cast<double>();
			auto y = tbl.GetItem("y").Cast<double>();
			printf("my_pos = (%f, %f)\n", x, y);
		}
	}

	printf("\n\nend\n");
}

void TestLunar()
{	
	auto luaState = lunar::LuaRunner::OpenState();
	if (luaState)
	{
		CustomScriptRegistrar::Register(luaState);

		lunar::LuaRunner::RunLuaFile(luaState, "test_lua_register.lua");

		lunar::LuaRunner::RunLuaFunction<void>(luaState, "luafun");
		auto sResVal1 = lunar::LuaRunner::RunLuaFunction<int>(luaState, "luafun1", 123);
		auto sResVal2 = lunar::LuaRunner::RunLuaFunction<std::string>(luaState, "luafun2", "hello", 456);


		lunar::LuaRunner::CloseState(luaState);

		size_t n = ObjectCounter::Count();
		printf("ObjectCounter::Count = %d \n", n);
	}

    std::cout << "Bye, bye...\n"; 

	size_t n = ObjectCounter::Count();
	printf("ObjectCounter::Count = %d \n", n);
}



int main()
{
	//TestLuaVar();

	TestLuaTable();

	//TestLunar();

	system("pause");
}

