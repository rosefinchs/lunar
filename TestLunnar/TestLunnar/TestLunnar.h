#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <math.h>
#include <assert.h>

//#include "lua.h"
#include "LuaRegister.h"

///////////////////////////////////////////////////////////////////////////

/// 为了导出map，使用新类包装一下std::map
/// STL 中的其他类也可以这样包装
/// 注：只能将C++中的map导出给lua，不能在lua中构造
template<typename K, typename V>
class LuaStlMap
{
	using StlMap = std::map<K, V>;
	using StlMapIterator = typename StlMap::iterator;
	using StlMapConstIterator = typename StlMap::const_iterator;

public:
	class Iterator
	{
	public:
		Iterator(const StlMapIterator& iter, const StlMapIterator& iterEnd)
			: m_Iterator(iter), m_IteratorEnd(iterEnd) {}
		Iterator(const StlMapConstIterator& iter, const StlMapConstIterator& iterEnd)
			: m_Iterator(iter), m_IteratorEnd(iterEnd) {}

		const K& Key() const { return m_Iterator->first; }
		const V& Value() const { return m_Iterator->second; }

		Iterator& Increase() { m_Iterator++; return *this; }

		bool Next() { m_Iterator++; return m_Iterator != m_IteratorEnd; }
		bool Equal(const Iterator& it) const { return m_Iterator == it.m_Iterator; }

		bool operator == (const Iterator& it) const { return Equal(it); }
		bool operator != (const Iterator& it) const { return !Equal(it); }

	private:
		StlMapConstIterator m_Iterator;
		StlMapConstIterator m_IteratorEnd;
	};


public:
	LuaStlMap(StlMap& src) : m_Map(src) {}
	LuaStlMap(const LuaStlMap& src) : m_Map(src.m_Map) {}

	StlMap& GetMap() { return m_Map; }
	const StlMap& GetMap() const { return m_Map; }

	size_t Size() const { return m_Map.size(); }
	bool Empty() const { return m_Map.empty(); }
	V& At(const K& k) { return m_Map.at(k); }
	const V& At(const K& k) const { return m_Map.at(k); }
	bool Has(const K& k) const { return m_Map.find(k) != m_Map.end(); }
	void Insert(const K& k, const V& v) { m_Map.insert({ k, v }); }
	void Clear() { m_Map.clear(); }

	Iterator Find(const K& k) const { return Iterator(m_Map.find(k), m_Map.end()); }
	Iterator Begin()const { return Iterator(m_Map.begin(), m_Map.end()); }
	Iterator End()const { return Iterator(m_Map.end(), m_Map.end()); }

private:
	LuaStlMap() {}
	StlMap& m_Map;
};

// STL 注册器
class LuaSTLRegister
{
public:

	// 注册 std::vector
	template<typename T>
	static void RegisterSTLVector(lua_State* ls, const char* name);

	// 注册 std::list
	template<typename T>
	static void RegisterSTLList(lua_State* ls, const char* name);

	// 注册 LuaStlMap
	template<typename K, typename V>
	static void RegisterLuaSTLMap(lua_State* ls, const char* name);
};

//////////////////////////////////////////////////////////////////////////

// 注册 std::vector
template<typename T>
void LuaSTLRegister::RegisterSTLVector(lua_State* ls, const char* name)
{
	lunar::LuaRegister lr(ls);

	typedef std::vector<T> TVector;
	lr.Class<TVector>(name)
		.Constructor()
		.Constructor("new")
		.Property("size", &TVector::size)
		.Property("is_empty", &TVector::empty)
		.Function("at", (T& (TVector::*)(const size_t))&TVector::at)
		.Function("push_back", (void (TVector::*)(const T&))&TVector::push_back)
		.Function("pop_back", &TVector::pop_back)
		.Function("clear", &TVector::clear)
		;
}

// 注册 std::list
template<typename T>
void LuaSTLRegister::RegisterSTLList(lua_State* ls, const char* name)
{
	lunar::LuaRegister lr(ls);

	typedef std::list<T> TList;
	lr.Class<TList>(name)
		.Constructor()
		.Constructor("new")
		.Property("size", &TList::size)
		.Property("is_empty", &TList::empty)
		.Property("front", (T&(TList::*)())&TList::front)
		.Property("back", (T&(TList::*)())&TList::back)
		.Function("push_front", (void (TList::*)(const T&))&TList::push_front)
		.Function("push_back", (void (TList::*)(const T&))&TList::push_back)
		.Function("pop_front", &TList::pop_front)
		.Function("pop_back", &TList::pop_back)
		.Function("clear", &TList::clear)
		;
}

// 注册 LuaStlMap
template<typename K, typename V>
void LuaSTLRegister::RegisterLuaSTLMap(lua_State* ls, const char* name)
{
	typedef LuaStlMap<K, V> KVMap;
	typedef KVMap::Iterator KVMapIterator;

	lunar::LuaRegister lr(ls);

	std::string iterName(name);
	iterName.append("_iterator");

	lr.Class<KVMapIterator>(iterName.c_str())
		.Property("key", &KVMapIterator::Key)
		.Property("value", &KVMapIterator::Value)
		.Function("increase", &KVMapIterator::Increase)
		.Function("equal", &KVMapIterator::Equal)
		;

	lr.Class<KVMap>(name)
		.Property("size", &KVMap::Size)
		.Property("is_empty", &KVMap::Empty)
		.Property("head", &KVMap::Begin) // head 和 tail 配对
		.Property("tail", &KVMap::End) // 不能在lua中叫end，因为end是lua关键字
		.Function("has", &KVMap::Has)
		.Function("find", &KVMap::Find)
		.Function("at", (V& (KVMap::*)(const K&))&KVMap::At)
		.Function("insert", &KVMap::Insert)
		.Function("clear", &KVMap::Clear)
		;
}

class ObjectCounter
{
public:
	ObjectCounter()
	{
		m_Count++;
		printf(" + ObjectCounter: count = %d \n" ,m_Count);
	}

	//ObjectCounter(const ObjectCounter& oc)
	//{
	//	*this = oc;
	//	m_Count++;
	//	printf(" + ObjectCounter(const ObjectCounter& oc)  \n");
	//}

	ObjectCounter(const std::string& name)
	{
		printf("!! ObjectCounter(const std::string& name)  \n");
		m_Count++;
	}

	virtual ~ObjectCounter()
	{
		m_Count--;
		printf(" - ObjectCounter: count = %d \n", m_Count);
	}

	static size_t Count()
	{
		return m_Count;
	}

private:
	static size_t m_Count;
};

size_t ObjectCounter::m_Count = 0;


struct Position : public ObjectCounter
{
	int x;
	int y;

	float len;

	Position()
	{
		x = y = 0;
		len = 0;
	}

	Position(int _x, int _y)
	{
		x = _x;
		y = _y;
		len = std::sqrt(x*x + y * y);
	}

	~Position()
	{
		//printf("C++: Position::~Position(): (%d,%d)\n", x, y);
	}
};

class Animal : public ObjectCounter
{
public:
	Animal()
	{
		m_Name = "Default";
	}

	Animal(const std::string& name)
	{
		m_Name = name;
	}

	std::string GetName() const
	{
		return m_Name;
	}

	void SetName(const std::string& name)
	{
		m_Name = name;
	}


	virtual void Bark()
	{
		printf("Animal %s barking: ~~~\n", m_Name.c_str());
	}

	Position pos;

protected:
	std::string m_Name;
};

class Cat : public Animal
{
public:
	Cat()
	{
		m_Name = "Kitty";
		printf("%s> Cat::Cat()  \n", m_Name.c_str());
	}
	Cat(const Cat& cat)
	{
		*this = cat;
		printf("%s> Cat::Cat(const Cat& cat)  \n", m_Name.c_str());
	}
	Cat(const std::string& name)
	{
		m_Name = name;
		printf("%s> Cat::Cat(const std::string&)  \n", m_Name.c_str());
	}
	~Cat()
	{
		printf("%s> Cat::~Cat() \n", m_Name.c_str());
	}

	virtual void Scratch(Animal* pAnimal)
	{
		assert(pAnimal);
		printf("Cat %s is scratching %s...\n", m_Name.c_str(), pAnimal->GetName().c_str());
	}
};

class Dog : public Animal
{
public:
	Dog() : m_Color("White")
	{
		//printf("%s> Dog::Dog()  \n", m_Name.c_str());
	}

	Dog(const Dog& dog)
	{
		*this = dog;
	}

	~Dog()
	{
		//printf("%s> Dog::~Dog() \n", m_Name.c_str());
	}

	Dog(const std::string& name) : m_Color("White")//,Animal(name)
	{
		SetName(name);

		//printf("%s> Dog::Dog(const std::string& ) \n", m_Name.c_str());
	}

	const char* GetColor() const { return m_Color; }
	void SetColor(char* c) { m_Color = c; }

	virtual void Bite(Animal& animal)
	{
		printf("Dog %s bitting %s....\n", m_Name.c_str(), animal.GetName().c_str());

	}

	virtual void Bark()
	{
		printf("Dog %s barking: woof~~~ woof~~~\n", m_Name.c_str());
	}

protected:
	const char* m_Color = nullptr;
};

typedef std::vector<Dog> DogVector;
typedef std::vector<Dog*> DogPtrVector;


class Husky : public Dog
{
	typedef std::map<std::string, std::string> StringStringMap;
public:
	Husky()
	{
		InitFriends();
		//printf("%s> Husky::Husky()  \n", m_Name.c_str());
	}
	Husky(const Husky& husky)
	{
		InitFriends();
		//printf("%s> Husky::Husky(const Husky& husky)  \n", m_Name.c_str());
	}

	Husky(const std::string& name) //: Dog(name) 
	{
		SetName(name);
		InitFriends();

		//printf("%s> Husky::Husky(const std::string& ) \n", m_Name.c_str());

	}

	~Husky()
	{
		InitFriends();
		//printf("%s> Husky::~Husky() \n", m_Name.c_str());
	}

	virtual void Bite(Animal& animal)
	{
		printf("Husky %s bitting %s....\n", m_Name.c_str(), animal.GetName().c_str());
	}

	void Bark() { printf("Husky %s barking: th~~~ th~~~\n", m_Name.c_str()); }

	DogVector& GetFriends()
	{
		return m_Friends;
	}

	DogVector& GetEnemies()
	{
		return m_Enemies;
	}

	LuaStlMap<std::string, DogPtrVector> GetOtherDogs()
	{
		return LuaStlMap<std::string, DogPtrVector>(m_OtherDogs);
	}

	LuaStlMap<std::string, std::string> GetFriendInfos()
	{
		return LuaStlMap<std::string, std::string>(m_FriendInfo);
	}

	LuaStlMap<std::string, Dog> GetSchoolMates()
	{
		return LuaStlMap<std::string, Dog>(m_SchoolMates);
	}

	void SetFriendInfos(const LuaStlMap<std::string, std::string>& infos)
	{
		m_FriendInfo = infos.GetMap();
	}

	Cat GetGrilfriend()
	{
		return Cat("Caitlyn");
	}

protected:
	void InitFriends()
	{
		m_Friends = { Dog("li"), Dog("zhang"), Dog("wang") };
		m_Enemies = { Dog("Hilter"), Dog("Carl"), Dog("Rose") };

		m_FriendInfo.insert({ "jim", "USA" });
		m_FriendInfo.insert({ "lilei", "China" });
		m_FriendInfo.insert({ "hayato", "Japan" });
		m_FriendInfo.insert({ "Kon", "Thai" });

		DogPtrVector friendDog = { new Dog("ying"), new Dog("chong"), new Dog("ling") };
		DogPtrVector enemyDog = { new Dog("ji"), new Dog("lao"), new Dog("zu") };

		m_OtherDogs["friend"] = friendDog;
		m_OtherDogs["enemy"] = enemyDog;

		m_SchoolMates["li"] = Dog("li");
		m_SchoolMates["zhang"] = Dog("zhang");
		m_SchoolMates["wang"] = Dog("wang");
	}

	DogVector m_Enemies;
	DogVector m_Friends;


	StringStringMap m_FriendInfo;

	std::map<std::string, Dog> m_SchoolMates;
	std::map<std::string, DogPtrVector> m_OtherDogs;
};


class Alaska : public Dog
{
public:
	Alaska() {}
	Alaska(const std::string& name) //: Dog(name)
	{
		SetName(name);
	}
};



void globalIntroAnimal(const Animal& animal)
{
	printf("globalIntroAnimal(C++): This is %s \n", animal.GetName().c_str());
}

std::string globalGetAnimalName(const Animal* pAnimal)
{
	assert(pAnimal);
	printf("globalGetAnimalName(C++): name = %s\n", pAnimal->GetName().c_str());
	return pAnimal->GetName();
}

Position g_pos(1234, 5678);
Position g_pos2(1111, 2222);
Position g_pos3(8888, 9999);


class CustomScriptRegistrar
{
public:

	static void Register(lua_State* ls)
	{
		AnimalRegister(ls);
	}

private:
	static void AnimalRegister(lua_State* ls)
	{
		LuaSTLRegister::RegisterSTLVector<Dog>(ls, "DogVector");
		LuaSTLRegister::RegisterSTLVector<Dog*>(ls, "DogPtrVector");

		LuaSTLRegister::RegisterLuaSTLMap<std::string, std::string>(ls, "StringStringMap");
		LuaSTLRegister::RegisterLuaSTLMap<std::string, Dog>(ls, "DogMap");
		LuaSTLRegister::RegisterLuaSTLMap<std::string, DogVector>(ls, "DogVectorMap");
		LuaSTLRegister::RegisterLuaSTLMap<std::string, DogPtrVector>(ls, "DogPtrVectorMap");

		lunar::LuaRegister lrg(ls);

		lrg.Class<ObjectCounter>("ObjectCounter")
			.StaticFunction("count", &ObjectCounter::Count)
			;

		lrg.Class<Position, ObjectCounter>("Position")
			.Constructor("new")
			.Constructor<int, int>("new2")
			.ReadWrite("x", &Position::x)
			.ReadWrite("y", &Position::y)
			.ReadOnly("len", &Position::len)
			;

		lrg.Function("g_intro_animal", &globalIntroAnimal);
		lrg.Function("g_get_animal_name", &globalGetAnimalName);


		lunar::LuaRegister lr(ls, "test");

		lr.ReadWrite("g_pos_rw", &g_pos);
		lr.ReadOnly("g_pos_r", &g_pos2);
		lr.Variant("g_pos_v", g_pos3);

		lr.Class<Animal, ObjectCounter>("Animal")
			.Constructor<const std::string>("new")
			.Property("name", &Animal::GetName, &Animal::SetName)
			.ReadWrite("pos", &Animal::pos)
			.Function("bark", &Animal::Bark)
			;

		lr.Class<Dog, Animal>("Dog")
			.Constructor<const std::string>("new")
			.Constructor<const std::string&>()
			.Property("color", &Dog::GetColor)
			.Function("bite", &Dog::Bite)
			;

		lr.Class<Cat, Animal>("Cat")
			.Constructor<const std::string>("new")
			.Constructor<const std::string&>()
			.Function("scratch", &Cat::Scratch)
			;

		lr.Class<Husky, Dog>("Husky")
			.Constructor()
			.Constructor<const std::string>("new")
			.Property("friends", &Husky::GetFriends)
			.Property("girlfriend", &Husky::GetGrilfriend)
			.Property("school_mates", &Husky::GetSchoolMates)
			.Property("friend_infos", &Husky::GetFriendInfos, &Husky::SetFriendInfos)
			.Property("others", &Husky::GetOtherDogs)
			;

		std::vector<Dog> myDogArray = { Dog("pang"), Dog("peng"), Dog("zhao"), Dog("hayato") };
		lunar::LuaRunner::CreateArray(ls, "my_dog_array", myDogArray);

		std::map<std::string, Dog> myDogMap = { {"new pants", Dog("peng")},{"the face", Dog("chen")},{"hegehog", Dog("cheng")},{"penicillin", Dog("xiao")} };
		lunar::LuaRunner::CreateMap(ls, "my_dog_band", myDogMap);

	}
};



