#pragma once

#include "LuaRegisterConfig.h"

namespace lunar
{

#define lua_clear(ls) lua_pop(ls, lua_gettop(ls))

#define LuaTypePointType(t, push_t)			\
	template<> struct LuaType<t*> {			\
		static inline void Push(lua_State* L, t* value) { push_t(L, *value); }		\
	};										\
	template<> struct LuaType<const t*> {	\
		static inline void Push(lua_State* L, const t* value) { push_t(L, *value); }\
	};										\
	template<> struct LuaType<t*&> {		\
		static inline void Push(lua_State* L, t* value) { push_t(L, *value); }		\
	};										\
	template<> struct LuaType<const t*&> {	\
		static inline void Push(lua_State* L, const t* value) { push_t(L, *value); }\
	};


	struct LuaNil
	{
		const static int Type = LUA_TNIL;
	};

	// 值类型
	template<typename T>
	struct LuaType
	{
		const static int Type = LUA_TUSERDATA;

		static inline T Get(lua_State* L, int idx) { return *lr::GetBoxedUserdata<T>(L, idx); }
		static inline bool Match(lua_State* L, int idx) { return true; }
		static inline void Push(lua_State* L, T value)
		{
			lr::PushBoxedValue(L, value);
			luaL_getmetatable(L, typeid(T).raw_name());
			lua_setmetatable(L, -2);
		}
	};

	// 指针类型
	template<typename T>
	struct lr::LuaType<T*>
	{
		const static int Type = LUA_TUSERDATA;

		static inline T* Get(lua_State* L, int idx) { return lr::GetBoxedUserdata<T>(L, idx); }
		static inline bool Match(lua_State* L, int idx) { return true; }
		static inline void Push(lua_State* L, T* value)
		{
			if (!value) { return lua_pushnil(L); }
			lr::PushBoxedPointer(L, value);
			luaL_getmetatable(L, typeid(*value).raw_name());
			lua_setmetatable(L, -2);
		}
	};

	// 指针的引用类型
	template<typename T> 
	struct lr::LuaType<T*&>
	{
		const static int Type = LUA_TUSERDATA;

		static inline T* Get(lua_State* L, int idx) { return lr::GetBoxedUserdata<T>(L, idx); }
		static inline bool Match(lua_State* L, int idx) { return true; }
		static inline void Push(lua_State* L, T*& value)
		{
			if (!value) { return lua_pushnil(L); }
			lr::PushBoxedPointer(L, value);
			luaL_getmetatable(L, typeid(*value).raw_name());
			lua_setmetatable(L, -2);
		}
	};

	// 引用类型
	template<typename T>
	struct lr::LuaType<T&>
	{
		const static int Type = LUA_TUSERDATA;

		static inline T& Get(lua_State* L, int idx) { return *lr::GetBoxedUserdata<T>(L, idx); }
		static inline bool Match(lua_State* L, int idx) { return true; }
		static inline void Push(lua_State* L, T& value)
		{
			lr::PushBoxedPointer(L, &value);
			luaL_getmetatable(L, typeid(T).raw_name());
			lua_setmetatable(L, -2);
		}
	};

	// 右值引用类型
	template<typename T>
	struct LuaType<T&&>
	{
		const static int Type = LUA_TUSERDATA;

		static inline T Get(lua_State* L, int idx) { return *lr::GetBoxedUserdata<T>(L, idx); }
		static inline bool Match(lua_State* L, int idx) { return true; }
		static inline void Push(lua_State* L, T&& value)
		{
			lr::PushBoxedValue(L, value);
			luaL_getmetatable(L, typeid(T).raw_name());
			lua_setmetatable(L, -2);
		}
	};

	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	// bool types
	template<> struct LuaType<bool> {
		const static int Type = LUA_TBOOLEAN;
		static inline void Push(lua_State* L, bool value) { lua_pushboolean(L, value); }
		static inline bool Match(lua_State* L, int idx) { return lua_type(L, idx) == LUA_TBOOLEAN; }
		static inline bool Get(lua_State* L, int idx) { return lua_toboolean(L, idx) != 0; }
	};
	template<> struct LuaType<bool&> : public LuaType<bool> {};
	template<> struct LuaType<const bool&> : public LuaType<bool> {};
	template<> struct LuaType<const bool> : public LuaType<bool> {};

	LuaTypePointType(bool, lua_pushboolean);

	// char types
	template<> struct LuaType<char> {
		const static int Type = LUA_TNUMBER;
		static inline void Push(lua_State* L, char value) { lua_pushinteger(L, value); }
		static inline bool Match(lua_State* L, int idx) { return lua_type(L, idx) == LUA_TNUMBER; }
		static inline char Get(lua_State* L, int idx) { return static_cast<char>(lua_tonumber(L, idx)); }
	};
	template<> struct LuaType<char&> : public LuaType<char> {};
	template<> struct LuaType<const char&> : public LuaType<char> {};
	template<> struct LuaType<const char> : public LuaType<char> {};

	// unsigned char types
	template<> struct LuaType<unsigned char> {
		const static int Type = LUA_TNUMBER;
		static inline void Push(lua_State* L, unsigned char value) { lua_pushinteger(L, value); }
		static inline bool Match(lua_State* L, int idx) { return lua_type(L, idx) == LUA_TNUMBER; }
		static inline unsigned char Get(lua_State* L, int idx) { return static_cast<unsigned char>(lua_tonumber(L, idx)); }
	};
	template<> struct LuaType<unsigned char&> : public LuaType<unsigned char> {};
	template<> struct LuaType<const unsigned char&> : public LuaType<unsigned char> {};
	template<> struct LuaType<const unsigned char> : public LuaType<unsigned char> {};

	// short types
	template<> struct LuaType<short> {
		const static int Type = LUA_TNUMBER;
		static inline void Push(lua_State* L, short value) { lua_pushinteger(L, value); }
		static inline bool Match(lua_State* L, int idx) { return lua_type(L, idx) == LUA_TNUMBER; }
		static inline short Get(lua_State* L, int idx) { return static_cast<short>(lua_tonumber(L, idx)); }
	};
	template<> struct LuaType<short&> : public LuaType<short> {};
	template<> struct LuaType<const short&> : public LuaType<short> {};
	template<> struct LuaType<const short> : public LuaType<short> {};

	LuaTypePointType(short, lua_pushinteger);

	// unsigned short types
	template<> struct LuaType<unsigned short> {
		const static int Type = LUA_TNUMBER;
		static inline void Push(lua_State* L, unsigned short value) { lua_pushinteger(L, value); }
		static inline bool Match(lua_State* L, int idx) { return lua_type(L, idx) == LUA_TNUMBER; }
		static inline unsigned short Get(lua_State* L, int idx) { return static_cast<unsigned short>(lua_tonumber(L, idx)); }
	};
	template<> struct LuaType<unsigned short&> : public LuaType<unsigned short> {};
	template<> struct LuaType<const unsigned short&> : public LuaType<unsigned short> {};
	template<> struct LuaType<const unsigned short> : public LuaType<unsigned short> {};

	LuaTypePointType(unsigned short, lua_pushinteger);

	// int types
	template<> struct LuaType<int> {
		const static int Type = LUA_TNUMBER;
		static inline void Push(lua_State* L, int value) { lua_pushinteger(L, value); }
		static inline bool Match(lua_State* L, int idx) { return lua_type(L, idx) == LUA_TNUMBER; }
		static inline int Get(lua_State* L, int idx) { return static_cast<int>(lua_tonumber(L, idx)); }
	};
	template<> struct LuaType<int&> : public LuaType<int> {};
	template<> struct LuaType<const int&> : public LuaType<int> {};
	template<> struct LuaType<const int> : public LuaType<int> {};

	LuaTypePointType(int, lua_pushinteger);

	// unsigned int types
	template<> struct LuaType<unsigned int> {
		const static int Type = LUA_TNUMBER;
		static inline void Push(lua_State* L, unsigned int value) { lua_pushinteger(L, value); }
		static inline bool Match(lua_State* L, int idx) { return lua_type(L, idx) == LUA_TNUMBER; }
		static inline unsigned int Get(lua_State* L, int idx) { return static_cast<unsigned int>(lua_tonumber(L, idx)); }
	};
	template<> struct LuaType<unsigned int&> : public LuaType<unsigned int> {};
	template<> struct LuaType<const unsigned int&> : public LuaType<unsigned int> {};
	template<> struct LuaType<const unsigned int> : public LuaType<unsigned int> {};

	LuaTypePointType(unsigned int, lua_pushinteger);

	// long types
	template<> struct LuaType<long> {
		const static int Type = LUA_TNUMBER;
		static inline void Push(lua_State* L, long value) { lua_pushinteger(L, value); }
		static inline bool Match(lua_State* L, int idx) { return lua_type(L, idx) == LUA_TNUMBER; }
		static inline long Get(lua_State* L, int idx) { return static_cast<long>(lua_tonumber(L, idx)); }
	};
	template<> struct LuaType<long&> : public LuaType<long> {};
	template<> struct LuaType<const long&> : public LuaType<long> {};
	template<> struct LuaType<const long> : public LuaType<long> {};

	LuaTypePointType(long, lua_pushinteger);

	// unsigned long types
	template<> struct LuaType<unsigned long> {
		const static int Type = LUA_TNUMBER;
		static inline void Push(lua_State* L, unsigned long value) { lua_pushinteger(L, value); }
		static inline bool Match(lua_State* L, int idx) { return lua_type(L, idx) == LUA_TNUMBER; }
		static inline unsigned long Get(lua_State* L, int idx) { return static_cast<unsigned long>(lua_tonumber(L, idx)); }
	};
	template<> struct LuaType<unsigned long&> : public LuaType<unsigned long> {};
	template<> struct LuaType<const unsigned long&> : public LuaType<unsigned long> {};
	template<> struct LuaType<const unsigned long> : public LuaType<unsigned long> {};

	LuaTypePointType(unsigned long, lua_pushinteger);

#ifdef _WIN64
	// size_t types
	template<> struct LuaType<size_t> {
		const static int Type = LUA_TNUMBER;
		static inline void Push(lua_State* L, size_t value) { lua_pushinteger(L, (lua_Integer)value); }
		static inline bool Match(lua_State* L, int idx) { return lua_type(L, idx) == LUA_TNUMBER; }
		static inline size_t Get(lua_State* L, int idx) { return static_cast<size_t>(lua_tointeger(L, idx)); }
	};
	template<> struct LuaType<size_t&> : public LuaType<size_t> {};
	template<> struct LuaType<const size_t&> : public LuaType<size_t> {};


	LuaTypePointType(size_t, lua_pushinteger);
#endif

	// float types
	template<> struct LuaType<float> {
		const static int Type = LUA_TNUMBER;
		static inline void Push(lua_State* L, float value) { lua_pushnumber(L, (lua_Number)value); }
		static inline bool Match(lua_State* L, int idx) { return lua_type(L, idx) == LUA_TNUMBER; }
		static inline float Get(lua_State* L, int idx) { return static_cast<float>(lua_tonumber(L, idx)); }
	};
	template<> struct LuaType<float&> : public LuaType<float> {};
	template<> struct LuaType<const float&> : public LuaType<float> {};
	template<> struct LuaType<const float> : public LuaType<float> {};

	LuaTypePointType(float, lua_pushnumber);

	// double types
	template<> struct LuaType<double> {
		const static int Type = LUA_TNUMBER;
		static inline void Push(lua_State* L, double value) { lua_pushnumber(L, (lua_Number)value); }
		static inline bool Match(lua_State* L, int idx) { return lua_type(L, idx) == LUA_TNUMBER; }
		static inline double Get(lua_State* L, int idx) { return static_cast<double>(lua_tonumber(L, idx)); }
	};
	template<> struct LuaType<double&> : public LuaType<double> {};
	template<> struct LuaType<const double&> : public LuaType<double> {};
	template<> struct LuaType<const double> : public LuaType<double> {};

	LuaTypePointType(double, lua_pushnumber);

	// character pointer types
	template<> struct LuaType<char*> {
		const static int Type = LUA_TSTRING;
		static inline void Push(lua_State* L, const char* value) { lua_pushstring(L, value); }
		static inline bool Match(lua_State* L, int idx) { return lua_type(L, idx) == LUA_TSTRING; }
		static inline const char* Get(lua_State* L, int idx) { return static_cast<const char*>(lua_tostring(L, idx)); }
	};
	template<> struct LuaType<const char*> : public LuaType<char*> {};


	template<> struct LuaType<std::string> {
		const static int Type = LUA_TSTRING;
		static inline void Push(lua_State* L, std::string value) { lua_pushstring(L, value.c_str()); }
		static inline bool Match(lua_State* L, int idx) { return lua_type(L, idx) == LUA_TSTRING; }
		static inline std::string Get(lua_State* L, int idx) { 
			const char* str = static_cast<const char*>(lua_tostring(L, idx)); 
			return str ? str : "";
		}
	};
	template<> struct LuaType<std::string&> : public LuaType<std::string> {};
	template<> struct LuaType<const std::string> : public LuaType<std::string> {};
	template<> struct LuaType<const std::string&> : public LuaType<std::string> {};

	template<> struct LuaType<std::string*> {
		const static int Type = LUA_TSTRING;
		static inline void Push(lua_State* L, std::string* value) { lua_pushstring(L, (*value).c_str()); }
	};
	template<> struct LuaType<const std::string*> : public LuaType<std::string*> {};

	// character array types
	template<int NUM_CHARS> struct LuaType<char[NUM_CHARS]> {
		const static int Type = LUA_TSTRING;
		static inline void Push(lua_State* L, const char value[NUM_CHARS]) { lua_pushstring(L, value); }
		static inline bool Match(lua_State* L, int idx) { return lua_type(L, idx) == LUA_TSTRING; }
		static inline const char* Get(lua_State* L, int idx) { return static_cast<const char*>(lua_tostring(L, idx)); }
	};
	template<int NUM_CHARS> struct LuaType<const char[NUM_CHARS]> {
		const static int Type = LUA_TSTRING;
		static inline void Push(lua_State* L, const char value[NUM_CHARS]) { lua_pushstring(L, value); }
		static inline bool Match(lua_State* L, int idx) { return lua_type(L, idx) == LUA_TSTRING; }
		static inline const char* Get(lua_State* L, int idx) { return static_cast<const char*>(lua_tostring(L, idx)); }
	};

	// nil type
	template<> struct LuaType<const LuaNil&> {
		const static int Type = LUA_TNIL;
		static inline void Push(lua_State* L, const LuaNil& value) { lua_pushnil(L); }
		static inline bool Match(lua_State* L, int idx) { return true; }
		static inline LuaNil Get(lua_State* L, int idx) { (void)L, (void)idx;  return LuaNil(); }
	};

	// c function type
	template<> struct LuaType<lua_CFunction> {
		const static int Type = LUA_TFUNCTION;
		static inline void Push(lua_State* L, const lua_CFunction value) { lua_pushcclosure(L, value, 0); }
		static inline bool Match(lua_State* L, int idx) { return true; }
		static inline lua_CFunction Get(lua_State* L, int idx) { return static_cast<lua_CFunction>(lua_tocfunction(L, idx)); }
	};

	// void types
	template<> struct LuaType<void*> {
		const static int Type = LUA_TLIGHTUSERDATA;
		static inline void Push(lua_State* L, const void* value) { lua_pushlightuserdata(L, (void*)value); }
		static inline bool Match(lua_State* L, int idx) { return lua_type(L, idx) == LUA_TLIGHTUSERDATA; }
		static inline void* Get(lua_State* L, int idx) { return static_cast<void*>(lua_touserdata(L, idx)); }
	};
	template<> struct LuaType<const void*> : public LuaType<void*> {};

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// 获取第一个Upvalue
	inline unsigned char* GetFirstUpvalueAsUserdata(lua_State* L)
	{
		void* buffer = lua_touserdata(L, lua_upvalueindex(1));
		return (unsigned char*)buffer;
	}

	enum { UserDate_CreateByCpp, UserDate_CreateByLua};

	// 将C++中指针压入lua堆栈
	template<typename T>
	inline void PushBoxedPointer(lua_State* ls, const T* ptr, bool createByLua=false)
	{
		int srcType = createByLua ? UserDate_CreateByLua : UserDate_CreateByCpp ;
		int* buffer = (int*)lua_newuserdata(ls, sizeof(T*) + sizeof(int));
		*buffer = srcType;

		// 指针头部有个int值，记录是否是lua中创建的
		memcpy(buffer + 1, &ptr, sizeof(T*));
	}

	// 将C++中值压入堆栈（自定义类，非基本类型）
	/// C++中将值入栈，无论作为参数还是返回值都是临时变量
	template<typename T>
	inline void PushBoxedValue(lua_State* ls, const T& val)
	{
		// 值传递，需要new一个对象并复制val，因为传过来的值可能是临时变量随时会被销毁
		// 理论上因为每个自定义类型都有__gc函数，lua垃圾回收的时候会回收这个new出来的对象
		// 需要进一步测试
		T* pT = new T(val);
		PushBoxedPointer(ls, pT, true);
	}

	// 获取栈中Userdata
	template<typename T>
	inline T* GetBoxedUserdata(lua_State* ls, int idx)
	{
		int* userdata = (int*)lua_touserdata(ls, idx);
		assert(userdata);

		// 指针头部有个int值，记录是否是lua中创建的
		return *(T**)(userdata + 1);
	}

	// 判断栈中UserData是否是lua中构造
	inline bool IsUserdataCreateByLua(lua_State* ls, int idx)
	{
		int* userdata = (int*)lua_touserdata(ls, idx);
		assert(userdata);

		// 指针头部有个int值，记录是否是lua中创建的
		int srcType = *userdata;
		return srcType == UserDate_CreateByLua;
	}
}