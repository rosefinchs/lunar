#pragma once

#include <stdio.h>
#include <map>
#include <string>
#include <vector>
#include <assert.h>
#include <windows.h>

// 在这里配置Lua的路径
extern "C"
{
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

//#include "luajit/lua.hpp"
}

namespace lunar {}
namespace lr = lunar;

namespace lunar
{

class Debug
{
public:
	// 在这里配置lua错误消息打印的位置（控制台、弹框、日志等）
	static void TraceOut(const char* format, ...)
	{
		char str_tmp[1024] = {0};

		va_list vArgList;                            
		va_start(vArgList, format);                
		_vsnprintf_s(str_tmp, 1024, 1024, format, vArgList);
		va_end(vArgList);

		printf("Lua Error: %s \n", str_tmp);

		//MessageBoxA(NULL, str_tmp, "Lua error!", 0);
	}

public:

	struct ItemInfo
	{
		std::string item_type;
		std::string item_value;
	};
	struct StackInfo
	{
		const char* top_type = nullptr;
		int stack_size = 0;

		std::vector<ItemInfo> stack_infos;
	};

	/// 打印lua中返回的错误信息
	static void TraceOutLuaError(lua_State* ls, const char* addin=nullptr)
	{
		TraceOut("error = %s, addin = %s", GetLuaError(ls), addin);
	}

	/// 获取lua中返回的错误信息
	static const char* GetLuaError(lua_State* ls)
	{
		const char* ret = "<unknown>";
		if (ls && lua_isstring(ls, -1))
		{
			ret = lua_tostring(ls, -1);
			lua_pop(ls, 1);
		}
		return ret;
	}

	/// 返回栈中元素的字符串形式（不转换栈中元素）
	static std::string GetStackString(lua_State* ls, int idx)
	{
		int luaType = lua_type(ls, idx);
		switch (luaType)
		{
		case LUA_TSTRING:
		{
			const char* svalue = lua_tostring(ls, idx);
			return svalue ? svalue : "";
		}
		case LUA_TBOOLEAN:
			return lua_toboolean(ls, idx) ? "true" : "false";
		case LUA_TNUMBER:
			return std::to_string(lua_tonumber(ls, idx));
		default:
			return lua_typename(ls, luaType);
		}

		return "";
	}

	/// 获取当前栈内的数据类型信息
	static StackInfo GetStackInfo(lua_State* ls)
	{
		StackInfo info;
		info.stack_size = lua_gettop(ls);

		int toptype = lua_type(ls, -1);
		info.top_type = lua_typename(ls, toptype);

		for (int i = -1; i >= -lua_gettop(ls); i--)
		{
			ItemInfo item;

			int itype = lua_type(ls, i);
			const char* stype = lua_typename(ls, itype);
			item.item_type = stype;

			if (itype== LUA_TTABLE)
			{
				lua_getfield(ls, i, "debug_name");
				const char* svalue = lua_tostring(ls, -1);
				item.item_value = svalue ? svalue : "";
				lua_pop(ls, 1);
			}
			else 
			{
				item.item_value = GetStackString(ls, i);
			}

			info.stack_infos.push_back(item);
		}

		return info;
	}
};
}