#pragma once

#include "LuaType.h"

namespace lunar
{
	// 执行lua中的函数
	template<typename RT>
	class LuaFunctorCaller
	{
	public:
		static void GetLuaFunctor(lua_State* ls, const char* func)
		{
			// 拷贝func字符串
			int sz = strlen(func) + 1;
			char* src = (char*)malloc(sz);
			memcpy(src, func, sz);

			// 根据"."拆分func字符串
			int table_count = -1;
			int idx = LUA_GLOBALSINDEX;
			char* p = strtok(src, ".");
			while (p)
			{
				lua_getfield(ls, idx, p);
				idx = -1;

				table_count++;
				p = strtok(NULL, ".");
			}

			if (table_count>0)
			{
				// 去掉中间的表，将function放到栈顶
				lua_replace(ls, -table_count - 1);
				lua_pop(ls, table_count - 1);
			}

			// 销毁临时创建的字符串
			free(src);
		}

		static void CallLuaFunction(lua_State* ls, int paramCount, const char* func)
		{
			//Debug::StackInfo info = Debug::GetStackInfo(ls);
			int result = lua_pcall(ls, paramCount, 1, 0);
			if (result != 0)
			{
				Debug::TraceOutLuaError(ls, func);
			}
			//info = Debug::GetStackInfo(ls);
		}

		static RT GetReturnValue(lua_State* ls)
		{
			static_assert(!std::is_void_v<RT>, "void function do not reurn any value.");

			RT&& ret = LuaType<RT>::Get(ls, -1);
			lua_pop(ls, 1);
			return ret;
		}

		static void LuaCall(lua_State* ls, const char* func)
		{
			GetLuaFunctor(ls, func);
			CallLuaFunction(ls, 0, func);
		}

		template<typename P1>
		static void LuaCall(lua_State* ls, const char* func, P1 p1)
		{
			GetLuaFunctor(ls, func);

			LuaType<P1>::Push(ls, p1);

			CallLuaFunction(ls, 1, func);
		}

		template<typename P1, typename P2>
		static void LuaCall(lua_State* ls, const char* func, P1 p1, P2 p2)
		{
			GetLuaFunctor(ls, func);

			LuaType<P1>::Push(ls, p1);
			LuaType<P2>::Push(ls, p2);

			CallLuaFunction(ls, 2, func);
		}

		template<typename P1, typename P2, typename P3>
		static void LuaCall(lua_State* ls, const char* func, P1 p1, P2 p2, P3 p3)
		{
			GetLuaFunctor(ls, func);

			LuaType<P1>::Push(ls, p1);
			LuaType<P2>::Push(ls, p2);
			LuaType<P3>::Push(ls, p3);

			CallLuaFunction(ls, 3, func);
		}

		template<typename P1, typename P2, typename P3, typename P4>
		static void LuaCall(lua_State* ls, const char* func, P1 p1, P2 p2, P3 p3, P4 p4)
		{
			GetLuaFunctor(ls, func);

			LuaType<P1>::Push(ls, p1);
			LuaType<P2>::Push(ls, p2);
			LuaType<P3>::Push(ls, p3);
			LuaType<P4>::Push(ls, p4);

			CallLuaFunction(ls, 4, func);
		}

		template<typename P1, typename P2, typename P3, typename P4, typename P5>
		static void LuaCall(lua_State* ls, const char* func, P1 p1, P2 p2, P3 p3, P4 p4, P5 p5)
		{
			GetLuaFunctor(ls, func);

			LuaType<P1>::Push(ls, p1);
			LuaType<P2>::Push(ls, p2);
			LuaType<P3>::Push(ls, p3);
			LuaType<P4>::Push(ls, p4);
			LuaType<P5>::Push(ls, p5);

			CallLuaFunction(ls, 5, func);
		}

		template<typename... P>
		static RT LuaCallN(lua_State* ls, const char* func, P... args)
		{
			static_assert(sizeof...(P)<=5, "Too Much paramters");

			LuaCall(ls, func, args...);

			if constexpr (!std::is_void_v<RT>)
			{
				return GetReturnValue(ls);
			}
		}
	};

	////////////////////////////////////////////////////////

	// 将c++函数打包为luac函数（有返回值）
	template<typename RT>
	class FunctionCallerImpl
	{
	public:
		static inline int Call(lua_State* ls, RT(*func)())
		{
			if constexpr(std::is_void_v<RT>)
			{
				func();
			}
			else
			{
				RT&& ret = func();
				LuaType<RT>::Push(ls, ret);
			}

			return 1;
		}

		template<typename P1>
		static inline int Call(lua_State* ls, RT(*func)(P1))
		{
			P1&& param = LuaType<P1>::Get(ls, 1);

			if constexpr (std::is_void_v<RT>)
			{
				func(param);
			}
			else
			{
				RT&& ret = func(param);
				LuaType<RT>::Push(ls, ret);
			}

			return 1;
		}

		template<typename P1, typename P2>
		static inline int Call(lua_State* ls, RT(*func)(P1, P2))
		{
			P1&& param1 = LuaType<P1>::Get(ls, 1);
			P2&& param2 = LuaType<P2>::Get(ls, 2);

			if constexpr (std::is_void_v<RT>)
			{
				func(param1, param2);
			}
			else
			{
				RT&& ret = func(param1, param2);
				LuaType<RT>::Push(ls, ret);
			}
			return 1;
		}

		template<typename P1, typename P2, typename P3>
		static inline int Call(lua_State* ls, RT(*func)(P1, P2, P3))
		{
			P1&& param1 = LuaType<P1>::Get(ls, 1);
			P2&& param2 = LuaType<P2>::Get(ls, 2);
			P3&& param3 = LuaType<P3>::Get(ls, 3);

			if constexpr (std::is_void_v<RT>)
			{
				func(param1, param2, param3);
			}
			else
			{
				RT&& ret = func(param1, param2, param3);
				LuaType<RT>::Push(ls, ret);
			}
			return 1;
		}

		template<typename P1, typename P2, typename P3, typename P4>
		static inline int Call(lua_State* ls, RT(*func)(P1, P2, P3, P4))
		{
			P1&& param1 = LuaType<P1>::Get(ls, 1);
			P2&& param2 = LuaType<P2>::Get(ls, 2);
			P3&& param3 = LuaType<P3>::Get(ls, 3);
			P4&& param4 = LuaType<P4>::Get(ls, 4);

			if constexpr (std::is_void_v<RT>)
			{
				func(param1, param2, param3, param4);
			}
			else
			{
				RT&& ret = func(param1, param2, param3, param4);
				LuaType<RT>::Push(ls, ret);
			}
			return 1;
		}

		template<typename P1, typename P2, typename P3, typename P4, typename P5>
		static inline int Call(lua_State* ls, RT(*func)(P1, P2, P3, P4, P5))
		{
			P1&& param1 = LuaType<P1>::Get(ls, 1);
			P2&& param2 = LuaType<P2>::Get(ls, 2);
			P3&& param3 = LuaType<P3>::Get(ls, 3);
			P4&& param4 = LuaType<P4>::Get(ls, 4);
			P5&& param5 = LuaType<P5>::Get(ls, 5);

			if constexpr (std::is_void_v<RT>)
			{
				func(param1, param2, param3, param4, param5);
			}
			else
			{
				RT&& ret = func(param1, param2, param3, param4, param5);
				LuaType<RT>::Push(ls, ret);
			}
			return 1;
		}

		//////////////////////////////////////////////////////

		template<typename Callee> 
		static inline int Call(lua_State* ls, Callee* callee, RT(Callee::*func)())
		{
			if constexpr (std::is_void_v<RT>)
			{
				(callee->*func)();
			}
			else
			{
				RT&& ret = (callee->*func)();
				LuaType<RT>::Push(ls, ret);
			}
			return 1;
		}

		template<typename Callee>
		static inline int Call(lua_State* ls, Callee* callee, RT(Callee::*func)()const)
		{
			if constexpr (std::is_void_v<RT>)
			{
				(callee->*func)();
			}
			else
			{
				RT&& ret = (callee->*func)();
				LuaType<RT>::Push(ls, ret);
			}
			return 1;
		}

		template<typename Callee, typename P1>
		static inline int Call(lua_State* ls, Callee* callee, RT(Callee::*func)(P1))
		{
			P1&& param = LuaType<P1>::Get(ls, 2);
			if constexpr (std::is_void_v<RT>)
			{
				(callee->*func)(param);
			}
			else
			{
				RT&& ret = (callee->*func)(param);
				LuaType<RT>::Push(ls, ret);
			}
			return 1;
		}

		template<typename Callee, typename P1>
		static inline int Call(lua_State* ls, Callee* callee, RT(Callee::*func)(P1)const)
		{
			P1&& param = LuaType<P1>::Get(ls, 2);
			if constexpr (std::is_void_v<RT>)
			{
				(callee->*func)(param);
			}
			else
			{
				RT&& ret = (callee->*func)(param);
				LuaType<RT>::Push(ls, ret);
			}
			return 1;
		}

		template<typename Callee, typename P1, typename P2>
		static inline int Call(lua_State* ls, Callee* callee, RT(Callee::*func)(P1, P2))
		{
			P1&& param1 = LuaType<P1>::Get(ls, 2);
			P2&& param2 = LuaType<P2>::Get(ls, 3);

			if constexpr (std::is_void_v<RT>)
			{
				(callee->*func)(param1, param2);
			}
			else
			{
				RT&& ret = (callee->*func)(param1, param2);
				LuaType<RT>::Push(ls, ret);
			}
			return 1;
		}

		template<typename Callee, typename P1, typename P2>
		static inline int Call(lua_State* ls, Callee* callee, RT(Callee::*func)(P1, P2)const)
		{
			P1&& param1 = LuaType<P1>::Get(ls, 2);
			P2&& param2 = LuaType<P2>::Get(ls, 3);

			if constexpr (std::is_void_v<RT>)
			{
				(callee->*func)(param1, param2);
			}
			else
			{
				RT&& ret = (callee->*func)(param1, param2);
				LuaType<RT>::Push(ls, ret);
			}
			return 1;
		}

		template<typename Callee, typename P1, typename P2, typename P3>
		static inline int Call(lua_State* ls, Callee* callee, RT(Callee::*func)(P1, P2, P3))
		{
			P1&& param1 = LuaType<P1>::Get(ls, 2);
			P2&& param2 = LuaType<P2>::Get(ls, 3);
			P3&& param3 = LuaType<P3>::Get(ls, 4);

			if constexpr (std::is_void_v<RT>)
			{
				(callee->*func)(param1, param2, param3);
			}
			else
			{
				RT&& ret = (callee->*func)(param1, param2, param3);
				LuaType<RT>::Push(ls, ret);
			}
			return 1;
		}

		template<typename Callee, typename P1, typename P2, typename P3>
		static inline int Call(lua_State* ls, Callee* callee, RT(Callee::*func)(P1, P2, P3)const)
		{
			P1&& param1 = LuaType<P1>::Get(ls, 2);
			P2&& param2 = LuaType<P2>::Get(ls, 3);
			P3&& param3 = LuaType<P3>::Get(ls, 4);

			if constexpr (std::is_void_v<RT>)
			{
				(callee->*func)(param1, param2, param3);
			}
			else
			{
				RT&& ret = (callee->*func)(param1, param2, param3);
				LuaType<RT>::Push(ls, ret);
			}
			return 1;
		}

		template<typename Callee, typename P1, typename P2, typename P3, typename P4>
		static inline int Call(lua_State* ls, Callee* callee, RT(Callee::*func)(P1, P2, P3, P4))
		{
			P1&& param1 = LuaType<P1>::Get(ls, 2);
			P2&& param2 = LuaType<P2>::Get(ls, 3);
			P3&& param3 = LuaType<P3>::Get(ls, 4);
			P4&& param4 = LuaType<P4>::Get(ls, 5);

			if constexpr (std::is_void_v<RT>)
			{
				(callee->*func)(param1, param2, param3, param4);
			}
			else
			{
				RT&& ret = (callee->*func)(param1, param2, param3, param4);
				LuaType<RT>::Push(ls, ret);
			}
			return 1;
		}

		template<typename Callee, typename P1, typename P2, typename P3, typename P4>
		static inline int Call(lua_State* ls, Callee* callee, RT(Callee::*func)(P1, P2, P3, P4)const)
		{
			P1&& param1 = LuaType<P1>::Get(ls, 2);
			P2&& param2 = LuaType<P2>::Get(ls, 3);
			P3&& param3 = LuaType<P3>::Get(ls, 4);
			P4&& param4 = LuaType<P4>::Get(ls, 5);

			if constexpr (std::is_void_v<RT>)
			{
				(callee->*func)(param1, param2, param3, param4);
			}
			else
			{
				RT&& ret = (callee->*func)(param1, param2, param3, param4);
				LuaType<RT>::Push(ls, ret);
			}
			return 1;
		}

		template<typename Callee, typename P1, typename P2, typename P3, typename P4, typename P5>
		static inline int Call(lua_State* ls, Callee* callee, RT(Callee::*func)(P1, P2, P3, P4, P5))
		{
			P1&& param1 = LuaType<P1>::Get(ls, 2);
			P2&& param2 = LuaType<P2>::Get(ls, 3);
			P3&& param3 = LuaType<P3>::Get(ls, 4);
			P4&& param4 = LuaType<P4>::Get(ls, 5);
			P5&& param5 = LuaType<P5>::Get(ls, 6);

			if constexpr (std::is_void_v<RT>)
			{
				(callee->*func)(param1, param2, param3, param4, param5);
			}
			else
			{
				RT&& ret = (callee->*func)(param1, param2, param3, param4, param5);
				LuaType<RT>::Push(ls, ret);
			}
			return 1;
		}

		template<typename Callee, typename P1, typename P2, typename P3, typename P4, typename P5>
		static inline int Call(lua_State* ls, Callee* callee, RT(Callee::*func)(P1, P2, P3, P4, P5)const)
		{
			P1&& param1 = LuaType<P1>::Get(ls, 2);
			P2&& param2 = LuaType<P2>::Get(ls, 3);
			P3&& param3 = LuaType<P3>::Get(ls, 4);
			P4&& param4 = LuaType<P4>::Get(ls, 5);
			P5&& param5 = LuaType<P5>::Get(ls, 6);

			if constexpr (std::is_void_v<RT>)
			{
				(callee->*func)(param1, param2, param3, param4, param5);
			}
			else
			{
				RT&& ret = (callee->*func)(param1, param2, param3, param4, param5);
				LuaType<RT>::Push(ls, ret);
			}
			return 1;
		}
	};

	////////////////////////////////////////////////////////

	// 将c++函数打包为luac函数
	class FunctorCaller
	{
	public:

		template<typename RT, typename... Args>
		static inline int FunctorParamCount(RT(*func)(Args...))
		{
			return sizeof...(Args);
		}

		template<typename Class, typename RT, typename... Args>
		static inline int MemberFunctorParamCount(RT(Class::*func)(Args...))
		{
			return sizeof...(Args);
		}

		template<typename Class, typename RT, typename... Args>
		static inline int MemberFunctorParamCount(RT(Class::*func)(Args...) const)
		{
			return sizeof...(Args);
		}

		////////////////////////////////////////////////////////

		template<typename RT, typename... Args>
		static inline int Call(lua_State* ls, RT(*func)(Args... args))
		{
			return FunctionCallerImpl<RT>::Call(ls, func);
		}

		template<typename RT, typename Callee, typename... Args>
		static inline int Call(lua_State* ls, Callee* callee, RT(Callee::*func)(Args... args))
		{
			return FunctionCallerImpl<RT>::Call(ls, callee, func);
		}

		template<typename RT, typename Callee, typename... Args>
		static inline int Call(lua_State* ls, Callee* callee, RT(Callee::*func)(Args... args)const)
		{
			return FunctionCallerImpl<RT>::Call(ls, callee, func);
		}
	};

	////////////////////////////////////////////////////////////


		// 构造函数（0-5个参数，可扩展）
	class LuaConstructor
	{
	public:
		template<typename Class>
		static int Constructor(lua_State* ls)
		{
			// lua中要求构造对象，申请内存，需要lua主动释放
			Class* pObj = new Class();
			if (!pObj)
			{
				return 1;
			}

			LuaConstructor::PushToStack<Class>(ls, pObj);
			return 1;
		}

		template<typename Class, typename P1>
		static int Constructor(lua_State* ls)
		{
			if (!LuaType<P1>::Match(ls, 1))
			{
				Debug::TraceOut("lua param match failed: %s", Debug::GetLuaError(ls));
				return 0;
			}

			P1 p1 = LuaType<P1>::Get(ls, 1);
			Class* pObj = new Class(p1);
			if (!pObj)
			{
				return 1;
			}

			LuaConstructor::PushToStack<Class>(ls, pObj);
			return 1;
		}

		template<typename Class, typename P1, typename P2>
		static int Constructor(lua_State* ls)
		{
			if (!LuaType<P1>::Match(ls, 1) || !LuaType<P2>::Match(ls, 2))
			{
				Debug::TraceOut("lua param match failed: %s", Debug::GetLuaError(ls));
				return 0;
			}

			P1 p1 = LuaType<P1>::Get(ls, 1);
			P2 p2 = LuaType<P2>::Get(ls, 2);

			Class* pObj = new Class(p1, p2);
			if (!pObj)
			{
				return 1;
			}

			LuaConstructor::PushToStack<Class>(ls, pObj);
			return 1;
		}

		template<typename Class, typename P1, typename P2, typename P3>
		static int Constructor(lua_State* ls)
		{
			if (!LuaType<P1>::Match(ls, 1) || !LuaType<P2>::Match(ls, 2) || !LuaType<P3>::Match(ls, 3))
			{
				Debug::TraceOut("lua param match failed: %s", Debug::GetLuaError(ls));
				return 0;
			}

			P1 p1 = LuaType<P1>::Get(ls, 1);
			P2 p2 = LuaType<P2>::Get(ls, 2);
			P3 p3 = LuaType<P3>::Get(ls, 3);

			Class* pObj = new Class(p1, p2, p3);
			if (!pObj)
			{
				return 1;
			}

			LuaConstructor::PushToStack<Class>(ls, pObj);
			return 1;
		}

		template<typename Class, typename P1, typename P2, typename P3, typename P4>
		static int Constructor(lua_State* ls)
		{
			if (!LuaType<P1>::Match(ls, 1) || !LuaType<P2>::Match(ls, 2) ||
				!LuaType<P3>::Match(ls, 3) || !LuaType<P4>::Match(ls, 4))
			{
				Debug::TraceOut("lua param match failed: %s", Debug::GetLuaError(ls));
				return 0;
			}

			P1 p1 = LuaType<P1>::Get(ls, 1);
			P2 p2 = LuaType<P2>::Get(ls, 2);
			P3 p3 = LuaType<P3>::Get(ls, 3);
			P4 p4 = LuaType<P4>::Get(ls, 4);

			Class* pObj = new Class(p1, p2, p3, p4);
			if (!pObj)
			{
				return 1;
			}

			LuaConstructor::PushToStack<Class>(ls, pObj);
			return 1;
		}

		template<typename Class, typename P1, typename P2, typename P3, typename P4, typename P5>
		static int Constructor(lua_State* ls)
		{
			if (!LuaType<P1>::Match(ls, 1) || !LuaType<P2>::Match(ls, 2) ||
				!LuaType<P3>::Match(ls, 3) || !LuaType<P4>::Match(ls, 4) ||
				!LuaType<P5>::Match(ls, 5))
			{
				Debug::TraceOut("lua param match failed: %s", Debug::GetLuaError(ls));
				return 0;
			}

			P1 p1 = LuaType<P1>::Get(ls, 1);
			P2 p2 = LuaType<P2>::Get(ls, 2);
			P3 p3 = LuaType<P3>::Get(ls, 3);
			P4 p4 = LuaType<P4>::Get(ls, 4);
			P5 p5 = LuaType<P5>::Get(ls, 5);

			Class* pObj = new Class(p1, p2, p3, p4, p5);
			if (!pObj)
			{
				return 1;
			}

			LuaConstructor::PushToStack<Class>(ls, pObj);
			return 1;
		}

		template<typename Class>
		static int Destruction(lua_State* ls)
		{
			// Destruction是lua中主动释放或者垃圾回收调用
			// lua中只能销毁lua中构造的对象，C++中的对象在C++中销毁
			// 即：只有LuaConstructor::Constructor中创建出来的对象才能在lua中释放
			bool createByLua = IsUserdataCreateByLua(ls, -1);
			if (!createByLua)
			{
				return 1;
			}

			Class* pObj = GetBoxedUserdata<Class>(ls, -1);
			if (pObj)
			{
				delete pObj;
			}
			return 1;
		}

		template<typename Class>
		static void PushToStack(lua_State* ls, Class* pObj)
		{
			assert(pObj);

			const char* typeId = typeid(Class).raw_name();

			PushBoxedPointer(ls, pObj, true);
			/// stack:
			///  userobject(pObj)

			// 获取metatable表
			luaL_getmetatable(ls, typeId);
			/// stack:
			///   metatable
			///   userobject(pObj)

			// 设置userdata 的metatable
			lua_setmetatable(ls, -2);
			/// stack:
			///   userobject(pObj)
		}
	};


	////////////////////////////////////////////////////////
	// 注册全局函数
	template <typename Func>
	class LuaGolbalFunctor
	{
	public:
		static inline int LuaCFunction(lua_State* ls)
		{
			// 获取此函数的第一个Upvalue，即函数指针，导出时设置
			unsigned char* buffer = GetFirstUpvalueAsUserdata(ls);
			Func* pFunc = (Func*)buffer;

			assert(pFunc);

			// 不再检查参数个数，因为lua中支持默认参数，如果参数不足会补充nil
			///int paramCount = lua_gettop(ls);
			///int funcParamCount = FunctorCaller::FunctorParamCount(*pFunc);
			///if (paramCount != funcParamCount)
			///{
			///	printf("Param Count NOT match(need %d, got %d).\n", funcParamCount, paramCount - 1);
			///	return 0;
			///}

			return FunctorCaller::Call(ls, *pFunc);
		}
	};

	//////////////////////////////////////////////////////////////

	// 注册成员函数
	template <typename Class, typename Func>
	class LuaClassFunctor
	{
	public:
		static int LuaCFunction(lua_State* ls)
		{
			const char* typeId = typeid(Class).raw_name();

			// 获取此函数的第一个Upvalue，即函数指针
			unsigned char* buffer = GetFirstUpvalueAsUserdata(ls);
			Func* pFunc = (Func*)buffer;

			assert(pFunc);

			// 不再检查参数个数，因为lua中支持默认参数，如果参数不足会补充nil
			///int paramCount = lua_gettop(ls);
			///int funcParamCount = FunctorCaller::MemberFunctorParamCount(*pFunc);
			///if (paramCount-1 != funcParamCount)
			///{
			///	printf("Param Count NOT match(need %d, got %d).\n", funcParamCount, paramCount-1);
			///	return 0;
			///}

			Class* pCallee = GetBoxedUserdata<Class>(ls, 1);
			assert(pCallee);
		
			return FunctorCaller::Call(ls, pCallee, *pFunc);
		}
	};

	// 注册成员变量
	template <typename Class, typename VType>
	class LuaMemberFunctor
	{
	public:
		static int LuaGetCFunction(lua_State* ls)
		{
			// 此函数的第一个upvalue是函数指针
			unsigned char* buffer = GetFirstUpvalueAsUserdata(ls);
			VType** ppMember = (VType**)buffer;
			assert(ppMember);

			Class* pCallee = GetBoxedUserdata<Class>(ls, 1);
			assert(pCallee);

			int ptrPos = reinterpret_cast<int>(pCallee) + reinterpret_cast<int>(*ppMember);
			VType* value = reinterpret_cast<VType*>(ptrPos);

			LuaType<VType*>::Push(ls, value);
			return 1;
		}

		static int LuaSetCFunction(lua_State* ls)
		{
			// 此函数的第一个upvalue是函数指针
			unsigned char* buffer = GetFirstUpvalueAsUserdata(ls);
			VType** ppMember = (VType**)buffer;
			assert(ppMember);

			VType value = LuaType<VType>::Get(ls, -1);

			Class* pCallee = GetBoxedUserdata<Class>(ls, 1);
			assert(pCallee);

			int ptrPos = reinterpret_cast<int>(pCallee) + reinterpret_cast<int>(*ppMember);
			VType* pVar = reinterpret_cast<VType*>(ptrPos);

			*pVar = value;
			return 1;
		}
	};

	// 注册模块中的变量
	template <typename VType>
	class LuaModuleVarFunctor
	{
	public:
		static int LuaGetCFunction(lua_State* ls)
		{
			// 此函数的第一个upvalue是全局变量指针
			unsigned char* buffer = GetFirstUpvalueAsUserdata(ls);
			VType** ppVar = (VType**)buffer;
			assert(ppVar);

			VType* pVar = *ppVar;
			assert(pVar);

			LuaType<VType*>::Push(ls, pVar);
			return 1;
		}

		static int LuaSetCFunction(lua_State* ls)
		{
			// 此函数的第一个upvalue是全局变量指针
			unsigned char* buffer = GetFirstUpvalueAsUserdata(ls);
			VType** ppVar = (VType**)buffer;
			assert(ppVar);

			VType value = LuaType<VType>::Get(ls, -1);

			VType* pVar = *ppVar;
			assert(pVar);

			*pVar = value;
			return 1;
		}
	};

	////////////////////////////////////////////////////////////
	// 模块metatable中的元方法
	class LuaModuleMetaFunctor
	{
	public:
		static int __index(lua_State* ls)
		{
			// 到module表中没找到元素，再到module的metatable的getter中找
	
			/// key
			/// table
			const char* key = lua_tostring(ls, -1);

			//lua_pushvalue(ls, -2);
			lua_getmetatable(ls, -2);
			/// table
			/// key
			/// table

			lua_pushstring(ls, "prop_getter");
			/// "prop_getter"
			/// table
			/// key
			/// table

			lua_rawget(ls, -2);
			/// getter table
			/// table
			/// key
			/// table

			// can't find gettertable
			if (lua_isnil(ls, -1))
			{
				Debug::TraceOut("Can't find key(%s).no getter tabel.\n", key);
				return 1;
			}

			lua_getfield(ls, -1, key);
			/// function
			/// table
			/// key
			/// table

			if (lua_isnil(ls, -1))
			{
				Debug::TraceOut("Error: Cant find key(%s) in getter table.\n", key);
				return 1;
			}

			int result = lua_pcall(ls,0, 1, 0);
			if (result!=0)
			{
				Debug::TraceOutLuaError(ls);
				return 0;
			}

			return 1;
		}

		static int __newindex(lua_State* ls)
		{
			// 到module表中找元素，先在module中rawset（不可触发__index）
			// 找不到再去module的metatable的setter中找

			/// value
			/// key
			/// table
			const char* key = lua_tostring(ls, -2);

			lua_getmetatable(ls, -3);
			/// metatable
			/// value
			/// key
			/// table

			lua_pushstring(ls, "prop_setter");
			/// "prop_getter"
			/// metatable
			/// value
			/// key
			/// table

			lua_rawget(ls, -2);
			/// getter table
			/// metatable
			/// value
			/// key
			/// table

			// can't find settertable
			if (lua_isnil(ls, -1))
			{
				Debug::TraceOut("Error: Cant find key(%s). no setter tabel.\n", key);
				return 1;
			}

			lua_getfield(ls, -1, key);
			/// function
			/// getter table
			/// metatable
			/// value
			/// key
			/// table

			if (lua_isnil(ls, -1))
			{
				Debug::TraceOut("Error: Cant find key(%s) in setter table.\n", key);
				return 1;
			}

			lua_pushvalue(ls, -4);
			/// value
			/// function
			/// getter table
			/// metatable
			/// value
			/// key
			/// table

			int result = lua_pcall(ls, 1, 0, 0);
			if (result != 0)
			{
				Debug::TraceOutLuaError(ls);
				return 0;
			}


			lua_clear(ls);
			return 1;
		}
	};

	////////////////////////////////////////////////////////////


	// 类metatable中的元方法
	class LuaClassMetaFunctor
	{
	public:
		static int __index(lua_State* ls)
		{
			const char* key = LuaType<const char*>::Get(ls, -1);

			// 第一次进入__index查找是从lua中，userdata调用的，栈底是userdata
			// 如果此userdata的metatable中没找到该元素，则会进入父类metatable中继续查找
			// 此时的栈底就是父类的metatable了。

			// 栈底是userdata
			if (lua_isuserdata(ls, 1))
			{
				///  key
				///  userdata

				lua_pushvalue(ls, 1);
				///  userdata
				///  key
				///  userdata

				lua_setfield(ls, LUA_REGISTRYINDEX, "__object");
				///  key
				///  userdata

				// 获取此类metatable
				if(lua_getmetatable(ls, -2)==0)
				{
					Debug::TraceOut("Find \"%s\" error. cant't find metatable.", key);
					return 1;
				}
				/// stack:
				///  metatable
				///  key
				///  userdata

				lua_replace(ls, 1);
				/// stack:
				///  key
				///  metatable
			}

			if(!lua_istable(ls, 1)) 
			{
				Debug::TraceOut("__index error");
				return 1;
			}

			/// statck:
			///  key
			///  metatable
		
			// 获取key值对应的value，放在栈顶
			lua_rawget(ls, -2);
			/// stack:
			///  value(or nil)
			///  metatable

			// metatable中找到了
			if (lua_type(ls, -1) != LUA_TNIL)
			{
				return 1;
			}
			else// metatable中没有找到，是属性吗？ 去metatable中的getter表里找
			{
				lua_pop(ls, 1);
			
				// 获取meta表中的getter表
				lua_getfield(ls, -1, "prop_getter");
				/// statck
				///  prop_getter table
				///  metatable

				// 从getter表中找到key对应的值（是个一个C函数）
				lua_getfield(ls, -1, key);
				/// statck:
				///  closure (or nil)
				///  prop_getter table
				///  metatable

				// getter表中找到了getter函数
				if (!lua_isnil(ls, -1))
				{
					// getter 表中函数格式为 T GetXXX(), 或者 T GetXXX() const;
					// 0个参数，1个返回值

					// 把函数放到栈底
					lua_insert(ls, 1);
					/// statck:
					///  prop_getter table
					///  metatable
					///  closure 

					// 去掉多余元素
					lua_settop(ls, 1);
					/// statck:
					///  closure 

					// 使用之前保存的userdata
					lua_getfield(ls, LUA_REGISTRYINDEX, "__object");
					/// stack:
					/// userdata
					/// closure

					// 调用栈里函数（1个参数，1个返回值）
					int result = lua_pcall(ls, 1, 1, 0);
					if (result!=0)
					{
						Debug::TraceOutLuaError(ls);
						return 0;
					}
					return 1;
				}
				else // getter 中也没有找到，到父对象里找
				{
					lua_pop(ls, 2);
					/// stack:
					///  metatable

					if(lua_getmetatable(ls, -1)==0)
					{
						// 没有父类metatable
						Debug::TraceOut("Can\'t find key \"%s\".\n", key);
						return 1;
					}
					/// stack:
					///  parent metatable
					///  metatable

					// 没有父类了
					if(lua_isnil(ls, -1))
					{
						Debug::TraceOut("Cant't find key \"%s\" .\n", key);
						return 1;
					}

					lua_replace(ls, 1);
					/// stack:
					///  parent metatable

					lua_pushstring(ls, key);
					/// stack:
					///  key
					///  parent metatable

					__index(ls);
				}	
			}

			return 1;
		}

		static int __newindex(lua_State* ls)
		{
			/// stack:
			///  value
			///  key
			///  userdata

			const char* key = LuaType<const char*>::Get(ls, -2);
			if (lua_isuserdata(ls, -3))
			{
				// 将栈底的userdata放到栈顶
				lua_pushvalue(ls, -3);
				/// stack:
				///  userdata
				///  value
				///  key
				///  userdata

				// 将栈顶userdata计入注册表
				lua_setfield(ls, LUA_REGISTRYINDEX, "__object");
				/// stack:
				///  value
				///  key
				///  userdata

				// 获取此类metatable
				if (lua_getmetatable(ls, 1) == 0)
				{
					Debug::TraceOut("Find %s error. cant't find metatable.", key);
					return 1;
				}
				/// stack:
				///  metatable
				///  value
				///  key
				///  userdata

				// 用metatable替换栈底的userdata
				lua_replace(ls, 1);
				/// stack:
				///  value
				///  key
				///  metatable
			}

			/// stack:
			///  value
			///  key
			///  metatable
			
			// 栈底此时应为metatable
			if (!lua_istable(ls, 1))
			{
				Debug::TraceOut("Can\'t find key \"%s\" .", key);
				return 1;
			}
			
			// metatable 放在栈顶
			lua_pushvalue(ls, -3);
			/// stack:
			///  metatable
			///  value
			///  key
			///  metatable

			// 压入key值
			lua_pushstring(ls, key);
			/// statck:
			///  key
			///  metatable
			///  value
			///  key
			///  metatable

			// 查找meta表中是否有此元素
			lua_rawget(ls, -2);
			/// statck:
			///  old value or nil
			///  metatable
			///  value
			///  key
			///  metatable

			// metatable中没有找到此元素，到setter中找
			if (lua_isnil(ls, -1))
			{
				// 弹出 nil
				lua_pop(ls, 1);

				// 获取meta表中的setter表
				lua_getfield(ls, -1, "prop_setter");
				/// statck
				///  prop_setter table
				///  metatable
				///  value
				///  key
				///  metatable

				// 从setter表中找到key
				lua_getfield(ls, -1, key);
				/// statck:
				///  closure (or nil)
				///  prop_setter table
				///  metatable
				///  value
				///  key
				///  metatable

				// setter表中有此key
				if(!lua_isnil(ls, -1)) 
				{
					// setter 表中函数格式为 void SetXXX(T), 或者 void SetXXX(T) const;
					// 1个参数 value，0个返回值

					/// statck:
					///  closure 
					///  prop_setter table
					///  metatable
					///  value
					///  key
					///  metatable

					// 把函数放到栈底
					lua_insert(ls, 1);
					/// statck:
					///  prop_setter table
					///  metatable
					///  value
					///  key
					///  metatable
					///  closure 

					// 去掉多余数据
					lua_settop(ls, 4);
					///  value
					///  key
					///  metatable
					///  closure 

					// 去掉key
					lua_replace(ls, 3);
					///  value
					///  metatable
					///  closure 

					// 去掉metatable
					lua_replace(ls, 2);
					///  value
					///  closure

					// 使用之前保存的userdata
					lua_getfield(ls, LUA_REGISTRYINDEX, "__object");
					/// stack:
					/// userdata
					/// value
					/// closure

					// 调整参数顺序
					lua_insert(ls, 2);
					/// stack:
					/// value
					/// userdata
					/// closure

					// 执行栈里函数（2个参数userdata, value， 无返回值）
					int result = lua_pcall(ls, 2, 0, 0);
					if (result!=0)
					{
						Debug::TraceOutLuaError(ls);
						return 1;
					}
				}
				else // setter 中也没有，到父类metatable中找
				{
					/// statck:
					///  nil
					///  prop_setter table
					///  metatable
					///  value
					///  key
					///  metatable

					// 弹出无用的值
					lua_pop(ls, 2);
					/// statck:
					///  metatable
					///  value
					///  key
					///  metatable
					
					// 获取父类metatable
					lua_getmetatable(ls, -1);
					/// statck:
					///  parent metatable
					///  metatable
					///  value
					///  key
					///  metatable

					if (lua_isnil(ls, -1))
					{
						Debug::TraceOut("Cant't find key \"%s\".\n", key);
						return 1;
					}

					lua_replace(ls, 1);
					/// statck:
					///  metatable
					///  value
					///  key
					///  parent metatable

					lua_pop(ls, 1);
					/// statck:
					///  value
					///  key
					///  parent metatable

					__newindex(ls);
				}
			}
			else // metatable 中找到了此元素
			{
				/// statck:
				///  old value
				///  metatable
				///  value
				///  key
				///  metatable

				// 弹出无用值
				lua_pop(ls, 2);
				/// statck:
				///  value
				///  key
				///  metatable

				// 设置metatable中key对应的value
				lua_rawset(ls, -3);
				/// stack:
				///  metatable
			}

			return 1;
		}

		template <typename Class, typename... P>
		static int __call(lua_State* ls)
		{
			// 调用class table 的 __call方法
			// stack:
			//	params...
			//	class table

			lua_remove(ls, 1);
			// stack:
			//	params...

			LuaConstructor::Constructor<Class, P...>(ls);
			return 1;
		}
	};

}