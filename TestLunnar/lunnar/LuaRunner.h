#pragma once
#include "LuaFunctor.h"
#include "LuaTable.h"

namespace lunar
{

	class LuaRunner
	{
	public:
		static lua_State* OpenState(bool loadLibs=true)
		{
			lua_State* ls = luaL_newstate();
			if (ls && loadLibs)
			{
				luaL_openlibs(ls);
			}

			return ls;
		}

		static void CloseState(lua_State* ls)
		{
			lua_close(ls);
		}

		static bool RunLuaFile(lua_State* ls, const std::string& file)
		{
			if (!ls)
			{
				return false;
			}

			try
			{
				_set_se_translator(ExecptionFunction);//设置捕获系统异常

				int ret = luaL_dofile(ls, file.c_str());
				if (ret)
				{
					Debug::TraceOut("lua do file(%s) failed! msg = %s \n", file.c_str(), Debug::GetLuaError(ls));
					return false;
				}
			}
			catch (...)
			{
				const char* errMsg = lua_tostring(ls, -1);
			}

			return true;
		}

		static bool RunLuaChunk(lua_State* ls, const std::string& chunk)
		{
			if (!ls)
			{
				return false;
			}

			try
			{
				_set_se_translator(ExecptionFunction);//设置捕获系统异常

				int ret = luaL_dostring(ls, chunk.c_str());
				if (ret)
				{
					Debug::TraceOut("lua do string(%s) failed! msg = %s \n", chunk.c_str(), Debug::GetLuaError(ls));
					return false;
				}
			}
			catch (...)
			{
				const char* errMsg = lua_tostring(ls, -1);
			}


			return true;
		}

		template<typename VT>
		static VT GetLuaVariant(lua_State* ls, const std::string& var)
		{
			lua_getfield(ls, LUA_GLOBALSINDEX, var.c_str());

			return LuaType<VT>::Get(ls, -1);
		}

		template<typename RT, typename... P>
		static RT RunLuaFunction(lua_State* ls, const std::string& func, P... args)
		{
			return LuaFunctorCaller<RT>::LuaCallN(ls, func.c_str(), args...);
		}

		template<typename KT, typename VT>
		static void CreateMap(lua_State* ls, const std::string& name, const std::map<KT,VT>& data)
		{
			if (!ls) { return; }

			lua_newtable(ls);
			for (auto p : data) {
				LuaType<KT>::Push(ls, p.first);
				LuaType<VT>::Push(ls, p.second);
				lua_settable(ls, -3);				
			}

			lua_setglobal(ls, name.c_str());
		}

		template<typename VT>
		static void CreateArray(lua_State* ls, const std::string& name, const std::vector<VT>& data)
		{
			if (!ls) { return; }

			lua_newtable(ls);
			for (int i=0; i<(int)data.size(); i++) {
				LuaType<int>::Push(ls, i+1);
				LuaType<VT>::Push(ls, data[i]);

				lua_settable(ls, -3);
			}

			lua_setglobal(ls, name.c_str());
		}

		template<typename KT, typename VT>
		static std::map<KT,VT> GetLuaMap(lua_State* ls, const std::string& name)
		{
			std::map<KT, VT> data;
			lua_getglobal(ls, name.c_str());

			lua_pushnil(ls);

			while( lua_next(ls, 1)!=0 ) {
				// 类型不匹配的跳过
				if(!LuaType<KT>::Match(ls, -2) || !LuaType<VT>::Match(ls, -1)) {
					lua_pop(ls, 1);
					continue;
				}

				VT&& val = LuaType<VT>::Get(ls, -1);
				KT&& key = LuaType<KT>::Get(ls, -2);
				data.insert({key, val});

				lua_pop(ls, 1);
			}

			lua_pop(ls, 1);
			return data;
		}

		template<typename VT>
		static std::vector<VT> GetLuaArray(lua_State* ls, const std::string& name)
		{
			std::vector<VT> data;
			lua_getglobal(ls, name.c_str());

			lua_pushnil(ls);

			while (lua_next(ls, 1) != 0) {
				VT&& val = LuaType<VT>::Get(ls, -1);
				data.push_back(val);

				lua_pop(ls, 1);
			}

			lua_pop(ls, 1);
			return data;
		}

		static LuaVar GetLuaVar(lua_State* ls, const std::string& name)
		{
			return LuaType<LuaVar>::GetGlobal(ls, name.c_str());
		}

		static LuaTable GetLuaTable(lua_State* ls, const std::string& name)
		{
			LuaVar var = LuaType<LuaVar>::GetGlobal(ls, name.c_str());
			if (!var.IsValid())
			{
				return LuaTable();
			}

			return var.Cast<LuaTable>();
		}
	private:
		static void ExecptionFunction(unsigned int u, EXCEPTION_POINTERS* pExp)
		{
			throw std::exception("system exception.");
		}
	};

}