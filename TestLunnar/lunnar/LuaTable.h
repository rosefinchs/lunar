#pragma once

#include "LuaType.h"

#include <memory>
#include <typeindex>


namespace lunar
{
	class LuaVar
	{
	public:

		template<typename T>
		LuaVar(T t) : m_data(new Data<T>(t))
		{
			m_lua_type = LuaType<T>::Type;	
		}

		LuaVar()
		{
			m_data = nullptr;
			m_lua_type = LUA_TNIL;
		}

		LuaVar(const LuaVar& v)
		{
			Free();

			m_data = v.Clone();
			m_lua_type = v.m_lua_type;
		}

		LuaVar(LuaVar&& v)
		{
			Free();

			*this = std::move(v);
		}

		~LuaVar()
		{
			Free();
		}

		bool IsValid()
		{
			return m_data!=nullptr;
		}

		LuaVar& operator = (const LuaVar& var)
		{
			Free();

			m_data = var.Clone();
			m_lua_type = var.m_lua_type;
			return *this;
		}

		bool operator <(const LuaVar& var) const
		{
			return m_data < var.m_data;
		}

		template<typename T>
		T Cast() const
		{
			assert(m_data);
			return dynamic_cast<Data<T>*>(m_data)->value;
		}


		int getLuaType() const
		{
			return m_lua_type;
		}

	private:
		void Free()
		{
			if (m_data)
			{
				delete m_data;
				m_data = nullptr;
			}
		}

	private:
		class Base 
		{
		public:
			virtual Base* Clone() const = 0;
			virtual ~Base() {}	
		};

		template <typename T>
		class Data :public Base 
		{
		public:
			Data(T t) : value(t) {}
			Base* Clone() const { return new Data<T>(*this); }

			T value;
		};

		Base* Clone() const
		{
			if (!m_data)
			{
				return nullptr;
			}

			return m_data->Clone();
		}

		Base* m_data = nullptr;

		int m_lua_type = LUA_TNIL;
	};


	void LuaVarTypePush(lua_State* L, LuaVar v);
	LuaVar LuaVarTypeGet(lua_State* L, int idx);
	LuaVar LuaVarTypeGetGlobal(lua_State* L, const char* name);


	template<> 
	struct LuaType<LuaVar> 
	{
		static inline bool Match(lua_State* L, int idx) { return true; }
		static inline void Push(lua_State* L, LuaVar v) { LuaVarTypePush(L, v); }
		static inline LuaVar Get(lua_State* L, int idx) { return LuaVarTypeGet(L, idx); }
		static inline LuaVar GetGlobal(lua_State* L, const char* name) { return LuaVarTypeGetGlobal(L, name); }
	};



	class LuaTable
	{
	public:
		LuaTable()
		{
		}

		LuaTable(lua_State* ls, int idx)
		{
			// idx处的table放入栈顶
			lua_pushvalue(ls, idx);

			Recurise(ls);
		}

		LuaTable(lua_State* ls, const std::string& name)
		{
			// 全局table压入栈顶
			lua_getglobal(ls, name.c_str());

			Recurise(ls);
		}

		size_t Size()
		{
			return m_Data.size();
		}

		bool Empty()
		{
			return m_Data.empty();
		}

		// 递归遍历
		void Recurise(lua_State* ls)
		{
			// nil 入栈作为初始 key
			lua_pushnil(ls);

			while (lua_next(ls, 1) != 0) 
			{
				// 现在栈顶（-1）是 value，-2 位置是对应的 key 

				// 类型匹配
				if (!LuaType<LuaVar>::Match(ls, -2) || !LuaType<LuaVar>::Match(ls, -1)) 
				{
					lua_pop(ls, 1);
					continue;
				}

				LuaVar val = LuaType<LuaVar>::Get(ls, -1);
				LuaVar key = LuaType<LuaVar>::Get(ls, -2);
				m_Data.insert({ key, val });

				// 弹出value， 留着key，继续next
				lua_pop(ls, 1);
			}

			lua_pop(ls, 1);
		}

		// table 入栈
		void Push(lua_State* L)
		{
			lua_createtable(L, 0, m_Data.size());
			for (const auto& p : m_Data)
			{
				
			}
		}

		// 获取table中的项目
		LuaVar GetItem(int index)
		{
			for (const auto& p : m_Data)
			{
				auto& key = p.first;
				auto& val = p.second;

				if (key.getLuaType()==LUA_TNUMBER)
				{
					if(index==key.Cast<int>())
					{
						return val;
					}
				}
			}

			return LuaVar();
		}

		// 获取table中的项目
		LuaVar GetItem(const char* name)
		{
			for (const auto& p : m_Data)
			{
				auto& key = p.first;
				auto& val = p.second;

				if (key.getLuaType() == LUA_TSTRING)
				{
					if (strcmp(name, key.Cast<const char*>())==0)
					{
						return val;
					}
				}
			}

			return LuaVar();
		}

		private:
			std::map<LuaVar, LuaVar> m_Data;
	};



	template<>
	struct LuaType<LuaTable>
	{
		const static int Type = LUA_TTABLE;

		static inline bool Match(lua_State* L, int idx) { return lua_type(L, idx)==LUA_TTABLE; }
		static inline void Push(lua_State* L, LuaTable v) { v.Push(L); }
		static inline LuaTable Get(lua_State* L, int idx) { return LuaTable(L, idx);}
	};


	static void LuaVarTypePush(lua_State* L, LuaVar v) {
		int t = v.getLuaType();
		switch (t)
		{
		case LUA_TNIL: lua_pushnil(L); break;
		case LUA_TBOOLEAN: lua_pushboolean(L, v.Cast<bool>()); break;
		case LUA_TLIGHTUSERDATA: lua_pushlightuserdata(L, v.Cast<void*>()); break;
		case LUA_TNUMBER: lua_pushnumber(L, v.Cast<double>()); break;
		case LUA_TSTRING: lua_pushstring(L, v.Cast<const char*>()); break;
		case LUA_TTABLE: v.Cast<LuaTable>().Push(L); break;
		case LUA_TFUNCTION: lua_pushcfunction(L, v.Cast<lua_CFunction>()); break;
		case LUA_TUSERDATA: lua_pushlightuserdata(L, v.Cast<void*>()); break;
		//case LUA_TTHREAD: lua_pushthread(L, v.Cast<lua_State*>()); break;
		default: lua_pushnil(L); break;
		}
	}

	static LuaVar LuaVarTypeGet(lua_State* L, int idx) {
		int t = lua_type(L, idx);
		switch (t)
		{
		case LUA_TNIL: return LuaVar();
		case LUA_TBOOLEAN: return LuaVar(lua_toboolean(L, idx) != 0);
		case LUA_TLIGHTUSERDATA: return LuaVar(lua_touserdata(L, idx));
		case LUA_TNUMBER: return LuaVar(lua_tonumber(L, idx));
		case LUA_TSTRING: return LuaVar(lua_tostring(L, idx));
		case LUA_TTABLE: return LuaVar(LuaTable(L, idx)); 
		case LUA_TFUNCTION: return LuaVar(lua_tocfunction(L, idx));
		case LUA_TUSERDATA: return LuaVar(lua_touserdata(L, idx));
		//case LUA_TTHREAD: return LuaVar(lua_tothread(L, idx));
		default: return LuaVar();
		}

		return LuaVar(nullptr);
	}

	static LuaVar LuaVarTypeGetGlobal(lua_State* L, const char* name) {
		lua_getglobal(L, name);

		//auto stackInfo = Debug::GetStackInfo(L);

		LuaVar var = LuaVarTypeGet(L, -1);

		lua_pop(L, 1);

		return var;
	}
}

