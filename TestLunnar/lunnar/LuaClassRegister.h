#pragma once 

#include "LuaType.h"
#include "LuaFunctor.h"

namespace lunar
{

	template<typename Class>
	class LuaClassRegister
	{
		friend class LuaRegister;

	public:
		// 注册枚举
		template<typename EType>
		LuaClassRegister<Class>& Enum(const char* name, EType e)
		{
			return def_var_to_classtable(name, (int)e);
		}

		// 用于直接把类表当函数用来构造对象
		template<typename... P>
		LuaClassRegister<Class>& Constructor()
		{
			// 获取类表
			get_class_table();
			/// stack:
			///  class table

			// 为这个类表设置metatable，metatable中记录的__call，可以使用这个类表作为函数使用，用来做构造函数

			std::string metaname = m_Name;
			metaname.append("_meta");

			// 创建一个metatable（如果已存在则不再创建 ）
			luaL_newmetatable(m_LuaState, metaname.c_str());
			/// stack:
			///  metatable
			///  classtable

			// 压入Key值"__call"
			lua_pushstring(m_LuaState, "__call");
			/// stack:
			///  "__call"
			///  metatable
			///  classtable

			lua_pushcclosure(m_LuaState, LuaClassMetaFunctor::__call<Class, P...>, 0);

			/// statck:
			/// closure(__call)
			/// "__call"
			/// metatable
			///  classtable

			// 设置metatable的__call属性为函数闭包
			lua_rawset(m_LuaState, -3);
			/// stack:
			///  metatable
			///  classtable

			// 设置classtable的metatable
			lua_setmetatable(m_LuaState, 1);
			/// stack:
			///  classtable

			lua_pop(m_LuaState, 1);
			/// stack empty

			return *this;
		}

		// 注册构造函数	
		template<typename... P>
		LuaClassRegister<Class>& Constructor(const char* name)
		{
			return def_func_to_classtable(name, LuaConstructor::Constructor<Class, P...>, 0, 0);
		}

		// 注册读写类型的成员变量
		/// member: 类成员变量指针
		template<typename VType>
		LuaClassRegister<Class>& ReadWrite(const char* name, VType Class::* member)
		{
			// 获取此类的metatable
			get_class_meta_table<Class>();
			/// stack:
			///  meta table

			//////////////////////////////////// 写入prop_getter表 ////////////////////////////////////////

			// 获取getter表
			lua_getfield(m_LuaState, -1, "prop_getter");
			/// stack:
			///  prop_getter table
			///  meta table

			// 函数名入栈
			lua_pushstring(m_LuaState, name);
			/// stack:
			///  name
			///  prop_getter table
			///  meta table

			// Upvalue 作为userdata入栈
			unsigned char* buffer = (unsigned char*)lua_newuserdata(m_LuaState, sizeof(member));
			memcpy(buffer, &member, sizeof(member));
			/// stack: 
			///  userdata(upvalue)
			///  name
			///  prop_getter table
			///  meta table

			// 闭包入栈
			lua_pushcclosure(m_LuaState, LuaMemberFunctor<Class, VType>::LuaGetCFunction, 1);
			/// stack:
			///  closure(closure吃掉了upvalue)
			///  name
			///  prop_getter table
			///  meta table

			// 放入getter表，名字为name
			lua_rawset(m_LuaState, -3);
			/// stack:
			///  prop_getter table
			///  meta table


			//////////////////////////////////// 写入prop_setter表 ////////////////////////////////////////

			// 弹出getter表
			lua_pop(m_LuaState, 1);
			/// stack:
			///  meta table

			// 获取setter表
			lua_getfield(m_LuaState, -1, "prop_setter");
			/// stack:
			///  prop_setter table
			///  meta table

			// 函数名入栈
			lua_pushstring(m_LuaState, name);
			/// stack:
			///  name
			///  prop_setter table
			///  meta table

			// Upvalue 作为userdata入栈
			buffer = (unsigned char*)lua_newuserdata(m_LuaState, sizeof(member));
			memcpy(buffer, &member, sizeof(member));
			/// stack: 
			///  userdata(upvalue)
			///  name
			///  prop_setter table
			///  meta table

			// 闭包入栈
			lua_pushcclosure(m_LuaState, LuaMemberFunctor<Class, VType>::LuaSetCFunction, 1);
			/// stack:
			///  closure(closure吃掉了upvalue)
			///  name
			///  prop_setter table
			///  meta table

			// 放入setter表，名字为name
			lua_rawset(m_LuaState, -3);
			/// stack:
			///  prop_setter table
			///  meta table
		

			lua_clear(m_LuaState);

			return *this;
		}

		// 注册只读类型的成员变量
		/// member: 类成员变量指针
		template<typename VType>
		LuaClassRegister<Class>& ReadOnly(const char* name, VType Class::* member)
		{
			// 获取此类的metatable
			get_class_meta_table<Class>();
			/// stack:
			///  meta table

			//////////////////////////////////// 写入prop_getter表 ////////////////////////////////////////

			// 获取getter表
			lua_getfield(m_LuaState, -1, "prop_getter");
			/// stack:
			///  prop_getter table
			///  meta table

			// 函数名入栈
			lua_pushstring(m_LuaState, name);
			/// stack:
			///  name
			///  prop_getter table
			///  meta table

			// Upvalue 作为userdata入栈
			unsigned char* buffer = (unsigned char*)lua_newuserdata(m_LuaState, sizeof(member));
			memcpy(buffer, &member, sizeof(member));
			/// stack: 
			///  userdata(upvalue)
			///  name
			///  prop_getter table
			///  meta table

			// 闭包入栈
			lua_pushcclosure(m_LuaState, LuaMemberFunctor<Class, VType>::LuaGetCFunction, 1);
			/// stack:
			///  closure(closure吃掉了upvalue)
			///  name
			///  prop_getter table
			///  meta table

			// 放入getter表，名字为name
			lua_rawset(m_LuaState, -3);
			/// stack:
			///  prop_getter table
			///  meta table

			lua_clear(m_LuaState);

			return *this;
		}
	
		// 注册成员函数
		/// fun: 类成员函数指针
		template<typename Func>
		LuaClassRegister<Class>& Function(const char* name, Func fun)
		{
			return def_func_to_metatable(name, LuaClassFunctor<Class, Func>::LuaCFunction, &fun, sizeof(fun));
		}

		// 注册lua_CFunction类型的函数
		LuaClassRegister<Class>& LuaCFunction(const char* name, lua_CFunction fun)
		{
			return def_func_to_metatable(name, fun, 0, 0);
		}

		// 注册只读类型属性（getter）
		template<typename FuncGetter>
		LuaClassRegister<Class>& Property(const char* name, FuncGetter getter)
		{
			// 获取此类的metatable
			get_class_meta_table<Class>();
			/// stack:
			///  meta table

			//////////////////////////////////// 写入prop_getter表 ////////////////////////////////////////

			// 获取getter表
			lua_getfield(m_LuaState, -1, "prop_getter");
			/// stack:
			///  prop_getter table
			///  meta table

			// 函数名入栈
			lua_pushstring(m_LuaState, name);
			/// stack:
			///  name
			///  prop_getter table
			///  meta table

			// Upvalue 作为userdata入栈
			unsigned char* buffer = (unsigned char*)lua_newuserdata(m_LuaState, sizeof(getter));
			memcpy(buffer, &getter, sizeof(getter));
			/// stack: 
			///  userdata(upvalue)
			///  name
			///  prop_getter table
			///  meta table

			// 闭包入栈
			lua_pushcclosure(m_LuaState, LuaClassFunctor<Class, FuncGetter>::LuaCFunction, 1);
			/// stack:
			///  closure(closure吃掉了upvalue)
			///  name
			///  prop_getter table
			///  meta table

			// 放入getter表，名字为name
			lua_rawset(m_LuaState, -3);
			/// stack:
			///  prop_getter table
			///  meta table

			lua_clear(m_LuaState);

			return *this;
		}

		// 注册读写类型属性（getter，setter）
		template<typename FuncGetter, typename FuncSetter>
		LuaClassRegister<Class>& Property(const char* name, FuncGetter getter, FuncSetter setter)
		{
			// 获取此类的metatable
			get_class_meta_table<Class>();
			/// stack:
			///  meta table

			//////////////////////////////////// 写入prop_getter表 ////////////////////////////////////////

			// 获取getter表
			lua_getfield(m_LuaState, -1, "prop_getter");
			/// stack:
			///  prop_getter table
			///  meta table

			// 函数名入栈
			lua_pushstring(m_LuaState, name);
			/// stack:
			///  name
			///  prop_getter table
			///  meta table

			// Upvalue 作为userdata入栈
			unsigned char* buffer = (unsigned char*)lua_newuserdata(m_LuaState, sizeof(getter));
			memcpy(buffer, &getter, sizeof(getter));
			/// stack: 
			///  userdata(upvalue)
			///  name
			///  prop_getter table
			///  meta table

			// 闭包入栈
			lua_pushcclosure(m_LuaState, LuaClassFunctor<Class, FuncGetter>::LuaCFunction, 1);
			/// stack:
			///  closure(closure吃掉了upvalue)
			///  name
			///  prop_getter table
			///  meta table

			// 放入getter表，名字为name
			lua_rawset(m_LuaState, -3);
			/// stack:
			///  prop_getter table
			///  meta table


			//////////////////////////////////// 写入prop_setter表 ////////////////////////////////////////

			// 弹出getter表
			lua_pop(m_LuaState, 1);
			/// stack:
			///  meta table

			// 获取setter表
			lua_getfield(m_LuaState, -1, "prop_setter");
			/// stack:
			///  prop_setter table
			///  meta table

			// 函数名入栈
			lua_pushstring(m_LuaState, name);
			/// stack:
			///  name
			///  prop_setter table
			///  meta table

			// Upvalue 作为userdata入栈
			buffer = (unsigned char*)lua_newuserdata(m_LuaState, sizeof(setter));
			memcpy(buffer, &setter, sizeof(setter));
			/// stack: 
			///  userdata(upvalue)
			///  name
			///  prop_setter table
			///  meta table

			// 闭包入栈
			lua_pushcclosure(m_LuaState, LuaClassFunctor<Class, FuncSetter>::LuaCFunction, 1);
			/// stack:
			///  closure(closure吃掉了upvalue)
			///  name
			///  prop_setter table
			///  meta table

			// 放入setter表，名字为name
			lua_rawset(m_LuaState, -3);
			/// stack:
			///  prop_setter table
			///  meta table

			lua_clear(m_LuaState);

			return *this;
		}
	
		// 注册静态成员函数，注册到类表里，在lua中使用Class.func来调用
		/// fun ： 类成员函数指针
		template<typename Func>
		LuaClassRegister<Class>& StaticFunction(const char* name, Func fun)
		{
			return def_func_to_classtable(name, LuaGolbalFunctor<Func>::LuaCFunction, &fun, sizeof(fun));
		}

	private:
		// 获得类表
		void get_class_table()
		{
			if (m_Moudle && m_Moudle[0])
			{
				// 获取模块表
				lua_getfield(m_LuaState, LUA_GLOBALSINDEX, m_Moudle);
				/// statck:
				///   moduletable

				// 获取此类表
				lua_getfield(m_LuaState, -1, m_Name);
				/// stack:
				///  class table
				///  moduletable

				// 去掉moduletable
				lua_replace(m_LuaState, -2);
				/// stack:
				///  class table
			}
			else
			{
				// 从全局表中获取此类表
				lua_getfield(m_LuaState, LUA_GLOBALSINDEX, m_Name);
				/// stack:
				///  class table
			}
		}

		// 获得类的metatable表
		template<typename Class>
		void get_class_meta_table()
		{
			const char* typeId = typeid(Class).raw_name();

			// 获取meta表
			luaL_getmetatable(m_LuaState, typeId);
			///  stack:
			///		metatable
		}

		// 注册析构函数
		// 注： 不能导出析构函数让lua主动析构对象，要让lua垃圾收集机制自动析构对象
		LuaClassRegister<Class>& destructor(const char* name)
		{
			return def_func_to_metatable(name, LuaConstructor::Destruction<Class>, 0, 0);
		}

		// 定义变量到类表
		template<typename VType>
		LuaClassRegister<Class>& def_var_to_classtable(const char* name, VType value)
		{
			// 获取类表
			get_class_table();
			/// stack:
			///  class table

			// 变量名入栈
			lua_pushstring(m_LuaState, name);
			/// stack:
			///  name
			///  classtable

			// 变量入栈
			LuaType<VType>::Push(m_LuaState, value);
			/// stack:
			///  value
			///  name
			///  classtable

			// 放入class表，名字为name
			lua_rawset(m_LuaState, -3);
			/// stack:
			///  classtable

			lua_clear(m_LuaState);
			
			return *this;
		}

		// 定义函数到metatable
		LuaClassRegister<Class>& def_func_to_metatable(const char* name, lua_CFunction cfunc, void* pfunc, int funcSize)
		{
			// 获取此类的metatable
			get_class_meta_table<Class>();
			/// stack:
			///  meta table

			// 函数名入栈
			lua_pushstring(m_LuaState, name);
			/// stack:
			///  name
			///  metatable

			if (pfunc && funcSize > 0)
			{
				// Upvalue 作为userdata入栈
				unsigned char* buffer = (unsigned char*)lua_newuserdata(m_LuaState, funcSize);
				memcpy(buffer, pfunc, funcSize);
				/// stack: 
				///  userdata(upvalue)
				///  name
				///  metatable

				// 闭包入栈
				lua_pushcclosure(m_LuaState, cfunc, 1);
				/// stack:
				///  closure(closure 吃掉了upvalue)
				///  name
				///  metatable
			}
			else
			{
				// 闭包入栈
				lua_pushcclosure(m_LuaState, cfunc, 0);
				/// stack:
				///  closure
				///  name
				///  metatable
			}

			// 放入metatable表，名字为name
			lua_rawset(m_LuaState, -3);
			/// stack:
			///  metatable

			lua_clear(m_LuaState);

			return *this;
		}

		// 定义函数到类表
		LuaClassRegister<Class>& def_func_to_classtable(const char* name, lua_CFunction cfunc, void* pfunc, int funcSize)
		{
			// 获取类表
			get_class_table();
			/// stack:
			///  classtable

			// 函数名入栈
			lua_pushstring(m_LuaState, name);
			/// stack:
			///  name
			///  classtable

			// Upvalue 作为userdata入栈
			unsigned char* buffer = (unsigned char*)lua_newuserdata(m_LuaState, funcSize);
			memcpy(buffer, pfunc, funcSize);
			/// stack: 
			///  userdata(upvalue)
			///  name
			///  classtable


			// 闭包入栈
			lua_pushcclosure(m_LuaState, cfunc, 1);
			/// stack:
			///  closure(closure吃掉了upvalue)
			///  name
			///  classtable

			// 放入class表，名字为name
			lua_rawset(m_LuaState, -3);
			/// stack:
			///  classtable

			lua_clear(m_LuaState);

			return *this;
		}

	public:
		~LuaClassRegister() {}

	private:
		LuaClassRegister(lua_State* ls, const char* module, const char* name)
		{
			m_LuaState = ls;
			m_Moudle = module;
			m_Name = name;

			// 垃圾回收调用析构函数
			destructor("__gc");
		}

	private:
		lua_State* m_LuaState = nullptr;
		const char* m_Moudle = nullptr;
		const char* m_Name = nullptr;
	};

}