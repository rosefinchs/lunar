#pragma once

#include "LuaType.h"
#include "LuaClassRegister.h"
#include "LuaRunner.h"


namespace lunar
{
	class LuaRegister
	{
	public:
		// 构造器，如果module存在，则创建一个模块表
		/// 此类注册的内容都是在这个模块表里，否则在全局空间内
		LuaRegister(lua_State* ls, const char* module=nullptr)
		{
			m_LuaState = ls;
			m_ModuleName = module;

			create_module_table();
		}

		// 注册 lua_CFunction 类型的函数
		LuaRegister& LuaCFunction(const char* name, lua_CFunction func)
		{
			// 获取模块表（如果有）
			if (push_module_table())
			{
				/// stack :
				///   module

				// 记录函数名
				lua_pushstring(m_LuaState, name);
				/// statck: 
				///   name
				///   module

				// 闭包入栈
				lua_pushcclosure(m_LuaState, func, 0);
				/// stack :
				///   closure
				///   name
				///   module

				lua_rawset(m_LuaState, -3);
				/// module

				lua_pop(m_LuaState, 1);
			}
			else
			{
				// 闭包入栈
				lua_pushcclosure(m_LuaState, func, 0);
				/// stack :
				///   closure

				// 将闭包记录到全局表中
				lua_setfield(m_LuaState, LUA_GLOBALSINDEX, name);
				/// stack: empty
			}

			return *this;
		}

		// 注册函数
		template<typename Func>
		LuaRegister& Function(const char* name, Func fun)
		{
			// 获取模块表（如果有）
			if (push_module_table())
			{
				/// stack :
				///   module

				// 记录函数名
				lua_pushstring(m_LuaState, name);
				/// statck: 
				///   name
				///   module

				// 函数指针作为闭包up值，封装到userdata，调用时再提取
				unsigned char* buffer = (unsigned char*)lua_newuserdata(m_LuaState, sizeof(fun));
				memcpy(buffer, &fun, sizeof(fun));
				/// stack:
				///  upvalue(userdata)
				///  name
				///  module

				// 闭包入栈(带一个upvalue)
				/// 将栈顶的1个元素作为upvalue封装到closure里，弹出1个栈顶元素，放入closure作为栈顶
				lua_pushcclosure(m_LuaState, LuaGolbalFunctor<Func>::LuaCFunction, 1);
				/// stack :
				///   closure
				///   name
				///   module

				// 记录闭包到module中
				lua_rawset(m_LuaState, -3);
				/// stack :
				///   module

				lua_pop(m_LuaState, 1);
			}
			else
			{
				// 函数指针作为闭包up值，封装到userdata，调用时再提取
				unsigned char* buffer = (unsigned char*)lua_newuserdata(m_LuaState, sizeof(fun));
				memcpy(buffer, &fun, sizeof(fun));
				/// stack:
				///  upvalue(userdata)

				// 闭包入栈(带一个upvalue)
				/// 将栈顶的1个元素作为upvalue封装到closure里，弹出1个栈顶元素，放入closure作为栈顶
				lua_pushcclosure(m_LuaState, LuaGolbalFunctor<Func>::LuaCFunction, 1);
				/// stack :
				///   closure

				// 将闭包记录到全局表中
				lua_setfield(m_LuaState, LUA_GLOBALSINDEX, name);
				/// stack: empty
			}

			return *this;
		}

		// 注册变量（值传递给lua，lua中的修改不会影响C++中的值）
		template<typename VType>
		LuaRegister& Variant(const char* name, VType v)
		{
			// 传值到lua中，lua中修改此值不会影响C++中的值

			//	获取module表（如果存在）
			if (push_module_table())
			{
				/// stack :
				///   module

				// 压入变量名
				lua_pushstring(m_LuaState, name);
				/// stack:
				///  name
				///  module

				// 压入变量值
				LuaType<VType>::Push(m_LuaState, v);
				/// stack:
				///  value
				///  name
				///  module

				// 记录值到module表
				lua_rawset(m_LuaState, -3);
				/// stack:
				///  module

				lua_pop(m_LuaState, 1);
				/// stack: empty

				//lua_setfield(m_LuaState, -1, name);
			}
			else
			{
				// 记录值到全局表
				LuaType<VType>::Push(m_LuaState, v);
				lua_setfield(m_LuaState, LUA_GLOBALSINDEX, name);
			}

			return *this;
		}

		// 注册读写类型的变量
		template<typename VType>
		LuaRegister& ReadWrite(const char* name, VType* v)
		{
			// getset 类型变量只能记录在模块里，不支持全局变量（因为要用到模块表的metatable，而全局表没有metatable）
			assert(m_ModuleName && m_ModuleName[0]);

			if (push_module_table())
			{
				/// module table

				// 获取module表的metatable
				lua_getmetatable(m_LuaState, -1);
				/// metatable
				/// module table

				// 压入getter表名
				lua_pushstring(m_LuaState, "prop_getter");
				/// "prop_getter"
				///  metatable
				///  module table

				// 获取getter表
				lua_rawget(m_LuaState, -2);
				/// getter table
				/// metatable
				/// module table

				// 压入变量名
				lua_pushstring(m_LuaState, name);
				/// name
				/// getter table
				/// metatable
				/// module

				// 创建一个userdata，记录变量指针
				unsigned char* buffer = (unsigned char*)lua_newuserdata(m_LuaState, sizeof(v));
				memcpy(buffer, &v, sizeof(v));
				/// userdata
				/// name
				/// getter table
				/// metatable
				/// module

				// 创建一个get函数，以userdata作为upvalue
				lua_pushcclosure(m_LuaState, LuaModuleVarFunctor<VType>::LuaGetCFunction, 1);
				/// closure
				/// name
				/// getter table
				/// metatable
				/// module

				// 在getter表中记录函数get函数
				lua_rawset(m_LuaState, -3);
				/// getter table
				/// metatable
				/// module

				
				////////////////////////////////////////////////////////////

				// 将变量指针写入metatable 的 setter 表里
				lua_pop(m_LuaState, 1);
				///  metatable
				///  module table

				// 压入setter表名字
				lua_pushstring(m_LuaState, "prop_setter");
				/// "prop_setter"
				///  metatable
				///  module table

				// 获取setter表
				lua_rawget(m_LuaState, -2);
				/// setter table
				/// metatable
				/// module table

				// 压入变量名
				lua_pushstring(m_LuaState, name);
				/// name
				/// setter table
				/// metatable
				/// module

				// 创建一个userdata记录变量的指针作为upvalue
				buffer = (unsigned char*)lua_newuserdata(m_LuaState, sizeof(v));
				memcpy(buffer, &v, sizeof(v));
				/// userdata
				/// name
				/// setter table
				/// metatable
				/// module

				// 压入set函数
				lua_pushcclosure(m_LuaState, LuaModuleVarFunctor<VType>::LuaSetCFunction, 1);
				/// closure
				/// name
				/// setter table
				/// metatable
				/// module

				// 将set函数记录到setter表中
				lua_rawset(m_LuaState, -3);
				/// setter table
				/// metatable
				/// module
				

				lua_clear(m_LuaState);
			}
			else
			{

				Debug::TraceOut("Can't support global read write(%s) .\n", name);
			}

			return *this;
		}

		// 注册只读类型的变量
		template<typename VType>
		LuaRegister& ReadOnly(const char* name, VType* v)
		{
			// getset 类型变量只能记录在模块里，不支持全局变量（因为要用到模块表的metatable，而全局表没有metatable）
			assert(m_ModuleName && m_ModuleName[0]);

			//	获取module表（如果存在）
			if (push_module_table())
			{
				/// metatable

				// 获取module表的metatable
				lua_getmetatable(m_LuaState, -1);
				/// metatable
				/// module table

				// 压入getter表名
				lua_pushstring(m_LuaState, "prop_getter");
				/// "prop_getter"
				///  metatable
				///  module table

				// 获取getter表
				lua_rawget(m_LuaState, -2);
				/// getter table
				/// metatable
				/// module table

				// 压入变量名
				lua_pushstring(m_LuaState, name);
				/// name
				/// getter table
				/// metatable
				/// module

				// 创建一个userdata记录变量指针作为upvalue
				unsigned char* buffer = (unsigned char*)lua_newuserdata(m_LuaState, sizeof(v));
				memcpy(buffer, &v, sizeof(v));
				/// userdata
				/// name
				/// getter table
				/// metatable
				/// module

				// 压入Get函数
				lua_pushcclosure(m_LuaState, LuaModuleVarFunctor<VType>::LuaGetCFunction, 1);
				/// closure
				/// name
				/// getter table
				/// metatable
				/// module

				// 记录get函数到getter表
				lua_rawset(m_LuaState, -3);
				/// getter table
				/// metatable
				/// module
			}
			else
			{
				Debug::TraceOut("Can't support global read write(%s) .\n", name);
			}

			lua_clear(m_LuaState);

			return *this;
		}

		// 注册类
		template<typename T>
		LuaClassRegister<T> Class(const char* name)
		{
			static_assert(std::is_copy_constructible_v<T>, "Class Must have copy constructor");

			assert(name && name[0]);

			// 如果此类不存在，则创建
			if (!find_class_table(name))
			{
				create_class_table<T>(name);

				create_meta_table<T>(name);

				create_prop_table<T>(name);
			}

			return LuaClassRegister<T>(m_LuaState, m_ModuleName, name);
		}

		// 注册继承类
		template<typename T, typename ParentT>
		LuaClassRegister<T> Class(const char* name)
		{
			static_assert(std::is_copy_constructible_v<T>, "Class Must have copy constructor");

			assert(name && name[0]);

			if (!find_class_table(name))
			{
				Class<T>(name);

				set_parent_metatable<T, ParentT>(name);
			}

			return LuaClassRegister<T>(m_LuaState, m_ModuleName, name);
		}

		// 注册枚举（在lua中统一为number类型）
		template<typename T>
		LuaRegister& Enum(const char* name, T item)
		{
			// 压入模块表
			if (push_module_table())
			{
				/// module table

				// 压入枚举名
				lua_pushstring(m_LuaState, name);
				/// name
				/// module table

				//　压入枚举值
				lua_pushinteger(m_LuaState, (int)item);
				/// item
				/// name
				/// module table

				// 记录枚举值到模块表
				lua_rawset(m_LuaState, -3);
				/// module table

				lua_pop(m_LuaState, 1);
				/// empty
			}
			else
			{
				//　压入枚举值
				lua_pushinteger(m_LuaState, (int)item);
				/// item

				// 记录枚举值到全局表
				lua_setfield(m_LuaState, LUA_GLOBALSINDEX, name);
				/// empty
			}

			return *this;
		}

	protected:
		//  查找此类表是否存在
		bool find_class_table(const char* name)
		{
			// 如果有模块名，压入模块名
			if (push_module_table())
			{
				/// stack:
				/// module

				// 压入表名
				lua_pushfstring(m_LuaState, name);
				/// stack:
				/// class name
				/// module

				// 获取类表
				lua_rawget(m_LuaState, -2);
				/// stack:
				/// class table or nil
				/// module
			}
			else
			{
				// 从全局表中获取此类表
				lua_getfield(m_LuaState, LUA_GLOBALSINDEX, name);
				/// stack:
				///  class table or nil
			}

			bool found = lua_istable(m_LuaState, -1);
			lua_clear(m_LuaState);
			
			return found;
		}

		//  创建一个类表，用于放置构造函数、静态函数、静态变量 
		template<typename Class>
		void create_class_table(const char* name)
		{
			// 如果有模块名，压入模块名
			if (push_module_table())
			{
				///   moduletable

				// 压入name
				lua_pushstring(m_LuaState, name);
				/// name
				/// moduletable

				// 创建一个新表
				lua_createtable(m_LuaState, 0, 0);
				/// stack: 
				///  new table
				///  name
				///  moduletable
#ifdef _DEBUG
				std::string classname;
				classname.append("class: ");
				classname.append(name);
				lua_pushstring(m_LuaState, classname.c_str());
				/// stack: 
				///  name
				///  new table
				///  name
				///  moduletable

				lua_setfield(m_LuaState, -2, "debug_name");
				/// stack: 
				///  new table
				///  name
				///  moduletable
#endif
				// 新表设置为module表的成员，名字为name
				lua_rawset(m_LuaState, -3);
				/// stack:
				/// moduletable

				lua_pop(m_LuaState, 1);
				/// empty
			}
			else
			{
				// 创建一个新表
				lua_createtable(m_LuaState, 0, 0);
				/// new table
#ifdef _DEBUG
				std::string classname;
				classname.append("class: ");
				classname.append(name);
				lua_pushstring(m_LuaState, classname.c_str());
				/// stack: 
				///  name
				///  new table

				lua_setfield(m_LuaState, -2, "debug_name");
				/// stack: 
				///  new table
#endif

				// 记录类表到全局表内
				lua_setfield(m_LuaState, LUA_GLOBALSINDEX, name);
				/// empty
			}
		}

		// 在meta表里创建两个表用于导出属性，表名分别是getter和setter
		template<typename T>
		void create_prop_table(const char* name)
		{
			/// 当lua中读取属性时，通过metatable的__index函数，
			///		判断如果getter表中有此属性对应的函数，则调用此函数
			/// 同理，当lua中给属性赋值，通过metatable的__newindex函数，
			///		判断如果setter中有此属性对应的函数，则调用此函数

			const char* typeId = typeid(T).raw_name();

			// 获取meta表
			luaL_getmetatable(m_LuaState, typeId);
			/// stack:
			///  metatable

			// 在meta表里创建一个Getter表
			lua_createtable(m_LuaState, 0, 0);
			/// stack:
			///  getter table
			///  metatable
#ifdef _DEBUG
			lua_pushstring(m_LuaState, "getter:");
			/// stack: 
			///  "prop_getter"
			///  getter table
			///  metatable

			lua_setfield(m_LuaState, -2, "debug_name");
			/// stack: 
			///  getter table
			///  metatable
#endif
			// 将Getter表记录在meta表中
			lua_setfield(m_LuaState, -2, "prop_getter");
			/// stack:
			///  metatable

			// 在meta表里创建一个Setter表
			lua_createtable(m_LuaState, 0, 0);
			/// stack:
			///  setter table
			///  metatable
#ifdef _DEBUG
			lua_pushstring(m_LuaState, "setter:");
			/// stack: 
			///  "setter"
			///  setter table
			///  metatable

			lua_setfield(m_LuaState, -2, "debug_name");
			/// stack: 
			///  setter table
			///  metatable
#endif
			// 将Setter表记录在meta表中
			lua_setfield(m_LuaState, -2, "prop_setter");
			/// stack:
			///  metatable

			// 清空栈
			lua_clear(m_LuaState);
		}

		// 创建此类的metatable表
		template<typename T>
		void create_meta_table(const char* classname)
		{
			const char* typeId = typeid(T).raw_name();

			// -------- 创建此类的的metatable表 ---------

			// 新建一个metatable表
			luaL_newmetatable(m_LuaState, typeId);
			/// stack:
			///	 metatable
#ifdef _DEBUG
			std::string name;
			name.append("metatable: ");
			name.append(classname);
			lua_pushstring(m_LuaState, name.c_str());
			/// stack: 
			///  name
			///	 metatable

			lua_setfield(m_LuaState, -2, "debug_name");
			/// stack: 
			///	 metatable
#endif
			/////////////////////////////////////////////////

			// 压入Key值"__index"
			lua_pushstring(m_LuaState, "__index"); // 用lua_pushliteral效率更好
			/// stack:
			///  "__index"
			///  metatable

			// 压入__index函数闭包
			lua_pushcclosure(m_LuaState, LuaClassMetaFunctor::__index, 0);
			/// statck:
			/// closure(__index)
			/// "__index"
			/// metatable

			// 设置metatable的__index属性为函数闭包
			lua_rawset(m_LuaState, -3); 
			/// stack:
			///  metatable

			/////////////////////////////////////////////////

			// 压入Key值"__newindex"
			lua_pushstring(m_LuaState, "__newindex"); // 用lua_pushliteral效率更好
			/// stack:
			///  "__newindex"
			///  metatable

			// 压入__index函数闭包
			lua_pushcclosure(m_LuaState, LuaClassMetaFunctor::__newindex, 0);
			/// statck:
			/// closure(__newindex)
			/// "__newindex"
			/// metatable

			// 设置metatable的__newindex属性为函数闭包
			lua_rawset(m_LuaState, -3);
			/// stack:
			///  metatable

			lua_clear(m_LuaState);
			/// stack empty
		}

		// 设置此类的metatable的metatable为父类的metatable
		template<typename T, typename ParentT>
		void set_parent_metatable(const char* name)
		{
			const char* typeId = typeid(T).raw_name();
			const char* parentTypeId = typeid(ParentT).raw_name();

			// 获取meta表
			luaL_getmetatable(m_LuaState, typeId);
			/// stack:
			///  metatable

			// 获取父类meta表
			luaL_getmetatable(m_LuaState, parentTypeId);
			/// stack:
			///  parent metatable
			///  metatable

			// 设置本类的meta表的metatable为父类meta表
			lua_setmetatable(m_LuaState, -2);
			/// stack:
			///  metatable
	
			lua_clear(m_LuaState);
		}

		// 取模块表（如果存在），压入栈顶
		bool push_module_table()
		{
			if ((m_ModuleName && m_ModuleName[0]))
			{
				// 获取模块表
				lua_getfield(m_LuaState, LUA_GLOBALSINDEX, m_ModuleName);
				/// stack :
				///   module
				return true;
			}

			return false;
		}

		// 创建模块表（如果存在）
		bool create_module_table()
		{
			if (!m_ModuleName || !m_ModuleName[0])
			{
				return false;
			}

			// 获取模块表
			lua_getfield(m_LuaState, LUA_GLOBALSINDEX, m_ModuleName);
			/// moduletable or nil

			// module已经存在，无需再次创建
			if (!lua_isnil(m_LuaState, -1))
			{
				lua_pop(m_LuaState, 1);
				return true;
			}

			lua_pop(m_LuaState, 1);

			// 新建一个module表
			lua_createtable(m_LuaState, 0, 0);
			/// module table

#ifdef _DEBUG
			std::string name;
			name.append("module: ");
			name.append(m_ModuleName);
			lua_pushstring(m_LuaState, name.c_str());
			/// stack: 
			///  name
			///	 module table

			lua_setfield(m_LuaState, -2, "debug_name");
			/// stack: 
			///	 module table
#endif
			// 新建一个metatable
			luaL_newmetatable(m_LuaState, m_ModuleName);
			/// metatable
			/// module table

			// 在metatable上新建一个getter表
			lua_createtable(m_LuaState, 0, 0);
			lua_setfield(m_LuaState, -2, "prop_getter");
			/// metatable
			/// module table

			// 在metatable上新建一个setter表
			lua_createtable(m_LuaState, 0, 0);
			lua_setfield(m_LuaState, -2, "prop_setter");
			/// metatable
			/// module table

			// 设置metatable的__index属性为函数闭包
			lua_pushstring(m_LuaState, "__index");
			lua_pushcclosure(m_LuaState, LuaModuleMetaFunctor::__index, 0);
			lua_rawset(m_LuaState, -3);
			/// metatable
			/// module table

			// 设置metatable的__newindex属性为函数闭包
			lua_pushstring(m_LuaState, "__newindex");
			lua_pushcclosure(m_LuaState, LuaModuleMetaFunctor::__newindex, 0);
			lua_rawset(m_LuaState, -3);
			/// metatable
			/// module table

			// 设置module表的metatable
			lua_setmetatable(m_LuaState, -2);
			/// module table

			// 将module表记录在全局表中
			lua_setfield(m_LuaState, LUA_GLOBALSINDEX, m_ModuleName);
			/// empty

			return true;
		}

	protected:
		lua_State* m_LuaState = nullptr;
		const char* m_ModuleName = nullptr;
	};


}