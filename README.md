# Lunar

#### 介绍
	a simple lua export library
	- export C++ class, function, variable to lua
	- run lua function in C++
	- C++ 17 or higher version

#### 软件架构
	TestLunar 
		- lua51 : lua5.1,  change to new version as you wish
		- lunar : lua export library
		- TestLunar : a sample show how to use lunar
		- test_lua_register.lua: used in TestLunar.


#### 安装教程
	Vs 2017 or higher version
	use C++ 17


#### 使用说明

	see TestLunar and test_lua_register.lua

#### 参与贡献

	rosefinchs


#### 特技

	- export C++ Class
	- export class member variant, member function, static function
	- export global function, global variant
	- export STL class or functions
	- do lua file in C++
	- run lua function in C++
	- get lua variant in C++
